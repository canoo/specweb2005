SPECweb2005 Release 1.01 Description
----------------------------------

SPEC has identified a code defect in the JSP implementation for the Banking
workload in SPECweb2005.  The defect in the JSP scripts results in the clients
not generating any of the required check_detail_image requests during the
benchmark. Instead, the code defect causes static requests for the check detail
image file to be generated in place of the dynamic requests that were part of
the design.

Since there are significant differences in how static and dynamic requests are
handled, SPEC is superceeding the v1.00 release with SPECweb2005 v1.01.

There are code changes to the client harness, JSP and PHP scripts, and minor
pseudcode updates.

Specifically, these changes include:

    o Updating the client harness (SPECweb_Banking.java) to handle errors
      in State 2 of the Banking workload correctly; when there is an error
      in the response, the harness will now return to state 1 instead of
      giving an erroneous error message about incorrect file size.

    o The banking JSP code (check_detail_html.jsp, check_detail_image.jsp,
      _header.jsp) has been updated to generate the required check_detail_image
      requests.  The "Cache-Control" header now consistently responds with
      "Cache-Control: no-cache"; this is now a requirement in the pseudocode.
      The TOMCAT.HOWTO has also been updated.

    o The banking PHP code (check_detail_image.php, inc.common.php) has been
      updated to remove "Content-type: image/jpeg" from check_detail_image.php
      to match the JSP implementation.  The "Cache-Control" header now
      consistently responds with "Cache-Control: no-cache";
      this is now a requirement in the pseudocode.

    o The banking pseudocode has removed the "Content-type: image/jpeg"
      header requirement.  The "Cache-Control: no-cache" header is now
      required in all responses.  Several minor typos were also fixed in
      the banking pseudocode.  All three workloads have been updated
      (banking_scripts_pseudocode.html, ecommerce_scripts_pseudocode.html,
      support_scripts_pseudocode.html).

SPEC requires that all SPECweb2005 published after September 6, 2005 use the
new Release 1.01.


Instructions for Installing the SPECweb2005 Release 1.01 Update Kit
-----------------------------------------------------------------

    o Rerun the installer (setup.jar or the Win32 EXE file) on all testbed
      components (prime client, clients, and SUT); BeSim is the exception, as
      it has not changed.  You can choose to install into a previous 1.00
      directory, although in this case you may want to back up your *.config files.

    o Be sure that no copies of the client are currently running.

    o Consider renaming your SPECweb2005-1.00 directories to SPECweb2005-1.01
      after the installation.
