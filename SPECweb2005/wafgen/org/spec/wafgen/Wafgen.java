//package wafgen.application;
/*
 * Wafgen.java
 *
 */
/*
 *
 * java Wafgen [properties_file:default=props.rc]
 *
 */

package org.spec.wafgen;

import java.io.*;
import java.util.*;
import java.lang.*;
//import wafgen.application.*;
/**
 *
 * @author  SmithPaula
 */
public class Wafgen {


//static final int fdcLength = 26;
public static int DEBUG = 0;
static final char NL = '\012';

//static String filesep = System.getProperty("file.separator");
static String filesep = "/";

// Reminder - this is declared static because there is only 1 instance in
// entire program - and its used in a static method.
static StringBuffer randomBuffer;
static String fixedFile = "props.rc";
static WafgenConfig config;

  /** Creates a new instance of Wafgen */
    public static void main(String[] args) {

        if (args.length > 0) {
	     String propsFilename = args[0];
             fixedFile = propsFilename;
        }
	else {
	    System.err.println
	        ("Error: Name of resource file (<workload>.rc) not specified on command line\n");
            System.exit(1);
	}


//--------------------------------------------------------------------------

	Random r = new Random(123456);

        try {
            config = WafgenConfig.readFrom(fixedFile);
            config.setValue("START_WAFGEN", new java.util.Date().toString());
            System.out.println("Start: " + config.getValue("START_WAFGEN"));
        } catch (Exception e) {
            System.err.println
                    ("Error: couldn't read properties file: exception "+e);
            ///e.printStackTrace();
            System.exit(1);
        }

	DEBUG = config.getDEBUG();

	/**
	 * Get parameters from property file
	 */

	if (config.getNumberOfConnections() <= 0) {
	    System.err.println ("Error: NumberOfConnections <= 0");
	    System.exit(1);
	}

	randomBuffer = new StringBuffer(config.getBigBufferSz());

	int TotalDirs = (int) (config.getBaseNumDirs() +
		               config.getNumberOfConnections() *
		               config.getDirScaling());

	int TotalSubDirs = (int) config.getSubDirScaling();

	if (config.getFirstDirectory() > TotalDirs) {
	    System.err.println("Error: FirstDirectory > TotalDirs ");
	    System.exit(1);
	}

	int firstdir = config.getFirstDirectory();
	int firstsubdir = config.getFirstSubDirectory();

	int maxdir = TotalDirs;
	if (firstdir == 0) { maxdir = maxdir - 1 ; }
	int maxsubdir = TotalSubDirs;
        if (firstsubdir  == 0) { maxsubdir = maxsubdir - 1 ; }
	//int lastdir = config.getLastDirectory();
        //if (lastdir > firstdir && lastdir <= TotalDirs)
	//	TotalDirs = lastdir +1 ;

	if (DEBUG >= 1) System.err.println ("TotalDirs:" + TotalDirs);

	String fulldirname = "." + filesep;
	String docrootname = config.getDocRoot();
	if (docrootname != null) fulldirname = docrootname;
        else {
	  docrootname = "." + filesep;
	  System.err.println 
	   ("Warning: DOCROOT not set, defaulting to current directory in 10 seconds.\n");
	  try { Thread.sleep(10000); } catch (InterruptedException e) { }
	  System.err.println ("Warning: continuing with DOCROOT as current directory.\n");
	  try { Thread.sleep(2000); } catch (InterruptedException e) { }
 
	}
        String basedirname = config.getBaseDirName();
        String basesubdirname = config.getBaseSubDirName();
	int classkeys = config.getConfigKeyCount("CLASS");
	if (classkeys != 0) {
	  if (basesubdirname == null && basedirname != null) {
		//fulldirname = fulldirname + filesep + basedirname;
	        String dirnum = String.valueOf(firstdir);
        	String adirname = basedirname.substring(0,
                         (basedirname.length() - dirnum.length()));
	        fulldirname = docrootname + filesep + adirname + dirnum;
	  }
	  else if (basesubdirname != null && basedirname != null) {
		//fulldirname = fulldirname + filesep + basesubdirname +
		//	filesep + basedirname;
	        String subdirnum = String.valueOf(firstsubdir);
		String adirname = basesubdirname.substring(0,
			(basesubdirname.length() - subdirnum.length()));
		fulldirname = docrootname + filesep + adirname + subdirnum;
  	  }
	} else {
	  if (basesubdirname == null && basedirname != null) {
		fulldirname = fulldirname + filesep + basedirname;
	  }
	  else if (basesubdirname != null && basedirname != null) {
		fulldirname = fulldirname + filesep + basesubdirname +
			filesep + basedirname;
  	  }
	}

	int fulldirnameLength = fulldirname.length();
	if (DEBUG >= 1) System.err.println ("fulldirname:" + fulldirname);
	File dDir  = new File (fulldirname.toString());
	if (fulldirnameLength > 0 ) {
	    dDir.mkdirs();
            if (!dDir.isDirectory()) {
                System.err.println ("Directory was not Created. \n");
                System.exit(0);
            }
	}


	//int classkeys = config.getConfigKeyCount("CLASS");
	if (classkeys != 0) {
	  if (basedirname == null) {
	    System.err.println ("Basedirname not set when CLASS used");
	    System.exit(0);
	  }
          // Create Empty Directories
          if ((TotalDirs > 1 || firstdir > 0 ) && (TotalSubDirs == 0)) {
	      for (int dir = firstdir; dir <= maxdir; dir++) {
	        String dirnum = String.valueOf(dir);
	        String adirname = basedirname.substring(0,
                         (basedirname.length() - dirnum.length()));
	        String newdirname = docrootname + filesep + adirname + dirnum;
	        if (DEBUG >= 1) System.err.println ("newdirname: " + newdirname);
	        dDir  = new File (newdirname.toString());
	        dDir.mkdirs();
                if (!dDir.isDirectory()) {
                    System.err.println ("Directory was not Created. \n");
                    System.exit(0);
                }
	      }
	  } else if ((TotalDirs > 1 || firstdir > 0 ) && (TotalSubDirs > 0)) {
	    int subdircount = 0;
	    for (int subdir = firstsubdir; subdir <= maxsubdir; subdir++) {
	        String subdirnum = String.valueOf(subdir);
		String adirname = basesubdirname.substring(0,
			(basesubdirname.length() - subdirnum.length()));
		String newsubdirname = docrootname + filesep + adirname +
			subdirnum;
		if (DEBUG >= 1) System.err.println
			("newsubdirname: " + newsubdirname);
		dDir  = new File (newsubdirname.toString());
		dDir.mkdirs();
                if (!dDir.isDirectory()) {
                    System.err.println ("Directory was not Created. \n");
                    System.exit(0);
                }
	    }
	    for (int dir = firstdir; dir <= maxdir; dir++) {
		int subdir = dir % TotalSubDirs;
	        if ((subdir == 0) && (firstsubdir != 0)) subdir = TotalSubDirs;
	        String subdirnum = String.valueOf(subdir);
	        String dirnum = String.valueOf(dir);
		String adirname = basesubdirname.substring(0,
			(basesubdirname.length() - subdirnum.length()));
		String newsubdirname = docrootname + filesep + adirname +
			subdirnum;
	        adirname = basedirname.substring(0,
                         (basedirname.length() - dirnum.length()));
	        String newdirname = newsubdirname+filesep+adirname+dirnum;
	        if (DEBUG >= 1) System.err.println("newdirname: "+newdirname);
	        dDir  = new File (newdirname.toString());
	        dDir.mkdirs();
                if (!dDir.isDirectory()) {
                    System.err.println ("Directory was not Created. \n");
                    System.exit(0);
                }
	    }

	}

	// Fill Directories with requested Classes
	/* Build random text buffer */
	buildRandomBuffer(config.getBigBufferSz());
        if ((TotalDirs > 1 || firstdir > 0 ) && (TotalSubDirs == 0)) {
	  for (int dir = firstdir; dir <= maxdir; dir++) {

	    /* Build random text buffer */
	    //buildRandomBuffer(config.getBigBufferSz());

            String newdirname = basedirname;
            if (TotalDirs > 1 || firstdir > 0 ) {
	      String dirnum = String.valueOf(dir);
	      String adirname = basedirname.substring(0,
                       (basedirname.length() - dirnum.length()));
	      newdirname = adirname + dirnum;
            }
  	    if (DEBUG >= 1) System.err.println ("newdirname: " + newdirname);

	      for ( int i = 0; i < classkeys; i++) {
	        String classbasename = config.getValue("CLASS[" + i + "]");
                int classlen=config.getClassLen(i);
                int classrel=config.getClassRel(i);
                int classincr=config.getClassIncr(i);
                int classcount=config.getClassCount(i);
		int classmincount=config.getClassMinCount(i);
		int classmaxcount=config.getClassMaxCount(i);
		int classdiv=config.getClassDiv(i);

	        if (classmaxcount != 0)
		    classcount =
                          r.nextInt(classmaxcount-classmincount)+classmincount;

	        for (int f=0; f < classcount; f++) {

		    String suffix = String.valueOf(f + classrel);
		    String ifileName = newdirname.toString() + filesep +
                      classbasename.substring
                      (0, classbasename.length() - suffix.length()) + suffix;

		    if (DEBUG >= 2) System.err.println ("file: " + ifileName);
		    int iFileSize = classlen + ((f + classrel) * classincr);
		    if ( classdiv != 0)
		        iFileSize = ( classlen * (f+1) ) /classdiv;
                    if (classmaxcount != 0 && classincr != 0)
			  iFileSize = classlen + r.nextInt(classincr);
		    create_file( i, f, iFileSize, ifileName, docrootname);

	        }
              }
            } // totalDirs

	  } else if ((TotalDirs > 1 || firstdir > 0 ) && (TotalSubDirs > 0)) {
	    /* Build random text buffer */
	    //  buildRandomBuffer(config.getBigBufferSz());
	    //Build files for subdirs
	    for (int dir = firstdir; dir <= maxdir; dir++) {

		int subdir = dir % TotalSubDirs;
	        if ((subdir == 0) && (firstsubdir != 0)) subdir = TotalSubDirs;
	        String subdirnum = String.valueOf(subdir);
	        String dirnum = String.valueOf(dir);
		String adirname = basesubdirname.substring(0,
			(basesubdirname.length() - subdirnum.length()));
		//String newsubdirname = docrootname + filesep + adirname +
		String newsubdirname = adirname + subdirnum;
	        adirname = basedirname.substring(0,
                         (basedirname.length() - dirnum.length()));
	        String newdirname = newsubdirname+filesep+adirname+dirnum;
              //String newdirname = basedirname;
              //if (TotalDirs > 1 || firstdir > 0 ) {
	      //  String dirnum = String.valueOf(dir);
	      //  String adirname = basedirname.substring(0,
              //         (basedirname.length() - dirnum.length()));
	      //  newdirname = adirname + dirnum;
              //}
  	      if (DEBUG >= 1) System.err.println ("Newdirname: " + newdirname);

	      for ( int i = 0; i < classkeys; i++) {
	        String classbasename = config.getValue("CLASS[" + i + "]");
                int classlen=config.getClassLen(i);
                int classrel=config.getClassRel(i);
                int classincr=config.getClassIncr(i);
                int classcount=config.getClassCount(i);
		int classmincount=config.getClassMinCount(i);
		int classmaxcount=config.getClassMaxCount(i);
		int classdiv=config.getClassDiv(i);

	        if (classmaxcount != 0)
		    classcount =
                          r.nextInt(classmaxcount-classmincount)+classmincount;

	        for (int f=0; f < classcount; f++) {

		    String suffix = String.valueOf(f + classrel);
		    String ifileName = newdirname.toString() + filesep +
                      classbasename.substring
                      (0, classbasename.length() - suffix.length()) + suffix;

		    if (DEBUG >= 2) System.err.println ("File: " + ifileName);
		    int iFileSize = classlen + ((f + classrel) * classincr);
		    if ( classdiv != 0)
		        iFileSize = ( classlen * (f+1) ) /classdiv;
                    if (classmaxcount != 0 && classincr != 0)
			  iFileSize = classlen + r.nextInt(classincr);
		    create_file( i, f, iFileSize, ifileName, docrootname);

	        }
              }
            } // totalDirs

          }
        } //classkeys != 0

        //  Create Individual Files
	int filekeys = config.getConfigKeyCount("FILE");
	String fname = "", flen = "", subdir = "", fdirname = "";
	if (filekeys != 0) {
	  if (DEBUG >= 2) System.err.println ("filekeys = " + filekeys);
	    /* Build random text buffer */
	    buildRandomBuffer(config.getBigBufferSz());
	    for ( int i = 0; i < filekeys; i++) {
	      String[] fileDefn = config.getFileDefn(i);
	      if (fileDefn.length > 0) fname = fileDefn[0];
              if (fileDefn.length > 1) flen = fileDefn[1];
              if (fileDefn.length > 2) subdir = fileDefn[2];
              int ifilelen = Integer.parseInt(flen);
              if (DEBUG >= 2) System.err.println ("fileDefn = " + fname + " " + subdir + " " + ifilelen);

	      if (subdir != null) {
	        fdirname = fulldirname + filesep  + subdir;
	        if (DEBUG >= 2) System.err.println ("fdirname: " + fdirname);
	        dDir  = new File (fdirname.toString());
	        dDir.mkdirs();
                if (!dDir.isDirectory()) {
                  System.err.println ("Directory was not Created. \n");
                  System.exit(0);
                }
              }
              String fileName = subdir+filesep+fname ;
	      if ( basedirname != null)
                  fileName = basedirname+filesep+subdir+filesep+fname ;

              // i = class# for incl, f = fileinclass - not used
	      create_file( i, -1, ifilelen, fileName, docrootname);
            }

      }

      config.setValue("COMPLETE_WAFGEN", new java.util.Date().toString());
      System.out.println("End: " + config.getValue("COMPLETE_WAFGEN"));
    }


    public static void create_file
     (int fclass, int inclass, int aFileSize, String fileName, String docRoot){
	int markerCt = 0;
	int inNameCt = 0;
	int i,j,k;
	char b;

        File aFile = new File (docRoot + filesep + fileName);
	if (DEBUG >= 3) System.err.println
	  ("aFile: " + (docRoot + filesep + fileName));

	java.util.Random generator = new
				java.util.Random (System.currentTimeMillis());

	int marker_freq = config.getMarkerFreq();
        int linesize = config.getLineSize();
        int bigbuffersz = config.getBigBufferSz();

	StringBuffer buf = new StringBuffer();

	String fSize = String.valueOf(aFileSize);

	buf.replace(0,(8 - fSize.length()), "        ");
	buf.replace((9 - fSize.length()), 8, fSize);
	buf.replace(9, 9, " ");

	if (DEBUG >= 3) System.err.println
	  (" " + fileName + " " + fclass + " " + inclass + " " + aFileSize);

	//String urlname=fileName.substring(fileName.length() - fdcLength)+"\n";
	String urlname = fileName + "\n";
        int line1Length = 9 + 1 + urlname.length();
	buf.append(urlname);

	j = bigbuffersz - (aFileSize + line1Length +
		generator.nextInt((bigbuffersz-aFileSize-line1Length)));

	if ( aFileSize < line1Length) {
	    System.err.println 
		("Error: path length too long for requested file size ");
	    System.exit(1);
	    //aFileSize = line1Length;
        }
	buf.append(randomBuffer.substring(j, j + aFileSize - line1Length));

	int incllocn = config.getClassInclStringLine(fclass);
	int inclctr  = config.getClassInclCounts(fclass);

	if (inclass == -1) {
	  incllocn = config.getFileInclStringLine(fclass);
	  inclctr  = config.getFileInclCounts(fclass);
        }

	// Setup incLocations[] to support placing multiple Incl lines
        // after randomly selected markers, at least 2 MUST be unique
	if ( incllocn == -1 && inclctr > 0) {
	  int[] incLocations = new int[inclctr];
          int next = 0;
	  if ((aFileSize/marker_freq) < 1) incLocations[0] = 0;
	  else incLocations[0] = generator.nextInt((aFileSize/marker_freq));
	  if (inclctr > 1) {
	    if ((aFileSize/marker_freq) < 1) next = 0;
            else next = generator.nextInt((aFileSize/marker_freq));
	    if (next > incLocations[0])
              if ((aFileSize/marker_freq) < 1) incLocations[1] = 0;
	      else incLocations[1] = generator.nextInt((aFileSize/marker_freq));
            else {
	      incLocations[1] = incLocations[0];
	      incLocations[0] = next;
	    }
            for (int l = 2; l < inclctr; l++) {
              incLocations[l] = -1;
            }
          }

	  while ( (inclctr > 1) && ((aFileSize/marker_freq) > 1) &&
				incLocations[1] == incLocations[0] ) {
	    next = generator.nextInt((aFileSize/marker_freq));
            if (next > incLocations[0])
              incLocations[1] = generator.nextInt((aFileSize/marker_freq));
            else {
              incLocations[1] = incLocations[0];
              incLocations[0] = next;
            }

	  }
	  if (inclctr > 2) {
            for (int l = 2; l < inclctr; l++) {
	      incLocations[l] = generator.nextInt((aFileSize/marker_freq));
            }
	  }
	  // Calculate the position to start paste of Incl String
	  for (int l = 0; l < inclctr; l++) {
	    int pastelen = config.getInclString().length();
	    if (pastelen > (linesize - 2)) {
              pastelen = linesize - 2;
	      System.err.println ("Warning: Incl String truncated");
	    }
	    String istring = config.getInclString().substring(0, pastelen);
            int paste =  incLocations[l] * marker_freq + linesize
				 -  pastelen - 1 + line1Length;
	    buf.replace(paste, pastelen+paste, istring);
					     //config.getInclString());
	  }
        } else if (incllocn > 1 && inclctr == 0) {
          // Place incl string on specified line before \n (can't be line 1)
	  // don't overwrite 1st char or last char
	  int pastelen = config.getInclString().length();
	  if (pastelen > (linesize - 2)) {
		pastelen = linesize - 2;
	        System.err.println ("Warning: Incl String truncated");
	  }
	  int paste = ((incllocn-1) * linesize) - pastelen - 1 + line1Length;
	  buf.replace(paste, pastelen+paste, config.getInclString().substring(0, pastelen));
	}

        for ( i=0, k=line1Length; i < (aFileSize - line1Length ); i++, k++) {
          if (0 == i%marker_freq){
            buf.setCharAt(k, urlname.charAt(markerCt));
            if (markerCt++ == (urlname.length()-2)) markerCt=0;
	  }
          else if ((linesize - 1) == (i % linesize)){
            buf.setCharAt(i + line1Length, '\n');
	  }
        }

	buf.setLength(aFileSize);

	if (DEBUG >= 4) System.out.print(buf);
	if (DEBUG >= 4) System.err.println(buf.length());
        try {
	    FileWriter out = new FileWriter(aFile);
	    out.write(buf.toString());
	    out.close();
        }
        catch (IOException e) {
            System.err.println
                    ("Error: couldn't write file: exception "+e);
            System.exit(0);
        }
    }


    public static void buildRandomBuffer (int bigbuffersz) {
           /* Create random text buffer */
            java.util.Random generator = new
                                java.util.Random (System.currentTimeMillis());
            for ( int i=0; i < bigbuffersz; i++) {
	      if (randomBuffer.length() == bigbuffersz)
                randomBuffer.setCharAt(i, (char) (generator.nextInt(92) + 33));
	      else
                randomBuffer.append((char) (generator.nextInt(92) + 33));
            }
    }

/* end of class */
}
