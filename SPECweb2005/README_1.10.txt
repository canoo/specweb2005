SPECweb2005 Release 1.10 Description
----------------------------------

SPECweb2005 Release 1.10 is a point release which adds the ability to use
multiple BeSim (backend simulators) servers, display statistics on the prime
client (either in CSV format on the console or as a graph).  Several minor
documentation updates have also been made.  In incorporates all the changes
from 1.01 and 1.02 (see README_1.01.txt and README_1.02.txt for details).

There are code changes to the client harness, JSP and PHP scripts, and minor
documentation updates.

Here is a detailed list of the code changes:

    o There are now multiple sets of .config files for different OS and script
      engine combinations provided to ease set-up on new installations.

    o The client harness now has the ability to poll clients for data at
      specified intervals during a run and display it as CSV or in a GUI.
      For details on using this feature, see section 3.4 of the User's Guide.

    o The PHP and JSP scripts now support multiple BeSims.  Requests will
      be distributed across them.  This is only necessary if your
      setup has a bottleneck (CPU, network, etc.) with one BeSim.
      
      To enable this, specify multiple BeSim machines as host/port pairs
      separated by spaces in Test.config, i.e.
      
      BESIM_SERVER = "besim1 besim2"
      BESIM_PORT = "81 82"
      
      This example will cause the scripts to access besim1 on port 81 and
      besim2 on port 82.

    o On short benchmark runs, or at very low numbers of simultaneous sessions,
      you'll often see errors related to not meeting the weighted percentage
      distributions for various pages.  If you don't want to see these errors,
      there is a new parameter in Test.config, IGNORE_WPD_ERRORS, that can
      be set to 1 to suppress these errors.

    o There is currently no formula that will determine the number of closes
      that are initiated by the server. Since these server-initiated closes
      require time-wait buffers, there is no way to determine the number of
      time-wait buffers required without knowing the number of times the
      server closed the connection.
      
      This information is included in the raw file in the following format:
      RESULT[x][y].NONCLIENT_CLOSE = "z"
      
    o A small fix to the banking BeSim code (besim_banking.c) has been made
      to prevent validation errors.

    o The BeSim directory now has Linux bash scripts which use cURL to do the
      sample requests, since the Perl test scripts require several CPAN
      modules that may not be on the system.

Here is a detailed list of the documentation changes:

    o The pseudocode has been updated with the requirement of accepting
      multiple BESIM_SERVERs/BESIM_PORTs from the prime client.  When
      there are multiple BeSims, the scripts should distribute
      the requests across them.
      
    o The run rules have been updated to simplify the process for submitting
      new dynamic code implementations (section 2.4).

    o The run rules have been updated to clearly state the requirements for
      SSL ciphers (sections 2.1.1, 3.2.2) and allowable software tunings (3.2.2.1).
    
    o The User's Guide has been updated with more instructions for setting
      up the PHP scripts (section 2.2).

    o The possibility of multiple BeSims, and the addition of bash scripts
      for testing BeSim operations has been added (section 2.3).

    o The User's Guide has been updated to state the new invocations needed
      for the client and prime client, as well as the need to pick a set of
      .config files and copy them to the appropriate filenames, along with
      some additional helpful tips (section 2.4).

    o The User's Guide has a new section on how to gather client statistics
      and display them on the prime client console as CSV on in a GUI
      (section 3.4).
      
Instructions for Installing the SPECweb2005 Release 1.10 Update Kit
-----------------------------------------------------------------

    o Rerun the installer (setup.jar or the Win32 EXE file) on all testbed
      components (prime client, clients, and SUT); BeSim is the exception, as
      it has not changed.  You can choose to install into a directory with a
      previous version, although in this case you should back up your *.config files.

    o Be sure that no copies of the client are currently running.

    o Consider renaming your SPECweb2005 directories to SPECweb2005-1.10
      after the installation.
