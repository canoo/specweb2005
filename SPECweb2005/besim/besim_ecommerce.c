/** Copyright (c) 2004-2005 Standard Performance Evaluation Corporation (SPEC)
 *               All rights reserved.
 */

/*

    SPECweb2005 Backend Simulator (BESIM)  Ecommerce Command Module 

*/

#include "besim_ecommerce.h"
#include "randomtext.h"
#include <math.h>

extern int randomtext (char* textbuf, int textlength, int texttype);


void DoEcommerceCommand(char* Arg[], int ArgCnt, char* MsgTxt){
  int Command;
  int Status;
  int AdjArgCnt;
  int fd;
  int s;
  char *ptr;
  struct stat FStats;

  /* Get Command Code and Adjusted Argument Count */

  Command = atoi(Arg[1]); 
  AdjArgCnt = ArgCnt - 1;
  ptr = MsgTxt;

  /* If ptr to globals have not been initialized by Reset command, then try
  /* to access them from the besim globals file via mmap/read.  This helps
  /* ensure that if the besim deamon dies and restarts or there are multiples
  /* that all the initialization values are available
  */
  if ((Command != 0) && ((gEConstants == NULL) ||
      ((stat(BESIM_ECOMMERCE_GLOBALS_PATH, &FStats) != -1) &&
       (FStats.st_mtime > gEConstants->lastResetTime)))) {
#ifndef WIN32
      fd = open( BESIM_ECOMMERCE_GLOBALS_PATH, O_RDWR, 0666 );
#else
      fd = _open( BESIM_ECOMMERCE_GLOBALS_PATH, O_RDWR, 0666 );
#endif
      if ( fd >= 0 ) {
        errno = 0;
#ifdef USE_MMAP
            gEConstants = mmap(NULL, sizeof(workload_globals_t),
              (PROT_WRITE|PROT_READ), (MAP_FILE|MAP_SHARED), fd, (off_t) 0 );
            if (gEConstants == MAP_FAILED) {
                ptr += sprintf(ptr, ERR1_STATUS);
                ptr += sprintf(ptr, "mmap: %d: %s\n", errno, strerror(errno));
                gEConstants = NULL;
#ifndef WIN32
                close(fd);
#else
                _close(fd);
#endif
                return;
            }
            if ((gEConstants->lastResetTime == 0) ||
                 (FStats.st_mtime > gEConstants->lastResetTime))
                        gEConstants->lastResetTime = FStats.st_mtime;
#else
	    gEConstants = &Constants;
	    s = read(fd, (void *) gEConstants, sizeof(workload_globals_t));
	    if (s != sizeof(workload_globals_t)) {
    	        ptr += sprintf(ptr, ERR1_STATUS);
                ptr += sprintf(ptr, "read: %d: %s\n", errno, strerror(errno));
                gEConstants = NULL;
#ifndef WIN32
                close(fd);
#else
                _close(fd);
#endif

	        return;
            }
            gEConstants->lastResetTime = FStats.st_mtime;
#endif
      } else {
     	   ptr += sprintf(ptr, ERR1_STATUS);
     	   strncpy(ptr, ERROR_BAD_GLOBALS, sizeof(ERROR_BAD_GLOBALS));
           return;
      }
#ifndef WIN32
      close(fd);
#else
      _close(fd);
#endif
  }

  /* Validate: Command Code, Argument Cound and 1st Argument for Command */
  /* Invoke Routine to process the specified command                     */
  
  if (Command >= 0 && Command <  (sizeof(ecommerce_cmdFp)/sizeof(ecommerce_cmdFp[0]))) {

   if (gEConstants != NULL && gEConstants->gflag == 9 && gEConstants->Load > 0) 
	srand((unsigned int) gEConstants->Load);
   
   if (AdjArgCnt >= ecommerce_cmdArgCnt[Command]) 
     Status = Ecommerce_Valid1stArg(Command, Arg[2], MsgTxt)? 
        (*ecommerce_cmdFp[Command])(&Arg[2], AdjArgCnt, MsgTxt): -1;
   else {
     ptr += sprintf(ptr, ERR1_STATUS);
     strncpy(ptr, ERROR_BAD_ARGS, sizeof(ERROR_BAD_ARGS));
   }
  }
  else {
    ptr += sprintf(ptr, ERR1_STATUS);
    strncpy(ptr, ERROR_UNDEF_CMD, sizeof(ERROR_UNDEF_CMD));
  }
  

}




int Ecommerce_Valid1stArg(int cmd, char* arg1, char* msg) {
    int firstarg;
    char *ptr;

    ptr = msg;

    /* Validate 1st argument for Ecommerce Commands, All commands */
    /* except Reset(0) should have a valid region id              */
    if (cmd == 11) { firstarg=gEConstants->Max_region;  return 1; }
    firstarg = atoi(arg1);
    if (cmd == 0 && firstarg != 0 ) return 1;
    if (VALID(firstarg,gEConstants->Min_region,gEConstants->Max_region)) return 1;
    else {
      ptr += sprintf(ptr, ERR1_STATUS);
      ptr += sprintf(ptr, "firstarg = %d, Min_region = %d, Max_region = %d\n",
		 firstarg,gEConstants->Min_region,gEConstants->Max_region);
      strncpy(ptr, ERROR_BAD_ARG1, sizeof(ERROR_BAD_ARG1));
    }
    return 0;
}


/*
  Ecommerce Command: 0 - Reset
  Request Params:  N_TIME&N_MinRegion&N_MaxRegion
  Response:		    S_Status (DONE)
  Script Mapping:  Prime Client Only
  DB Op:		    NA
*/

int Ecommerce_Reset (char* Arg[], int ArgCnt, char* MsgTxt){
    
    int len = 0;
    time_t now;
    const struct tm *tmstruct;
    char *ptr;
    int fd;
    struct stat FStats;

    ptr = MsgTxt;

    /* Get YYYYMMDD and return to Prime Client */
    /* May be used for Date fields in response and/or validation */

    (void)setlocale(LC_ALL, "");
    time(&now);
    tmstruct = localtime(&now);
    strftime(Constants.ResetDate, 16, "%Y%m%d", tmstruct);

    /* Save values for use in subsequent commands */
    Constants.Time = atoi(Arg[0]);
    Constants.Min_region = atoi(Arg[1]);
    Constants.Max_region = atoi(Arg[2]);
    Constants.region_range = Constants.Max_region - Constants.Min_region + 1 ;
    Constants.Load = atoi(Arg[3]);
    Constants.Scaled_Load = (int) (sqrt(atof(Arg[3]))/2.0);
    if (Constants.Scaled_Load == 0) Constants.Scaled_Load = 1;
    Constants.gflag = 9;
    Constants.lastResetTime = 0;

    /* save the globals to file for use by other Besim deamons */
    unlink (BESIM_ECOMMERCE_GLOBALS_PATH);
    fd = open( BESIM_ECOMMERCE_GLOBALS_PATH, O_RDWR|O_CREAT, 0666 );
    if ( fd >= 0 ) {
        write (fd, &Constants, sizeof(workload_globals_t));
        fsync(fd);
        gEConstants = &Constants;
        if (-1 == stat( BESIM_ECOMMERCE_GLOBALS_PATH, &FStats) ) {
                ptr += sprintf(ptr, ERR1_STATUS);
                strncpy(ptr, ERROR_BAD_GLOBALS, sizeof(ERROR_BAD_GLOBALS));
#ifndef WIN32
                close(fd);
#else
                _close(fd);
#endif
                return 0;
        } else {
                lseek (fd, 0, SEEK_SET);
                gEConstants->lastResetTime = FStats.st_mtime;
                write (fd, &Constants, sizeof(workload_globals_t));
                fsync(fd);
#ifndef WIN32
                sleep (1);
#else
		Sleep (1000);
#endif
        }
#ifndef WIN32
        close(fd);
#else
        _close(fd);
#endif


    } else {
        ptr += sprintf(ptr, ERR1_STATUS);
     	strncpy(ptr, ERROR_BAD_GLOBALS, sizeof(ERROR_BAD_GLOBALS));
	return 0;
    }

    ptr += sprintf(ptr, OK_STATUS);
    sprintf(ptr,
      "DONE ResetDate = %s, Time=%d,Min_UID=%d,Max_UID=%d,Load=%d,SL=%d\n",
      gEConstants->ResetDate, gEConstants->Time, gEConstants->Min_region, 
      gEConstants->Max_region, gEConstants->Load, gEConstants->Scaled_Load);

    return 0;
}


		  
/*
  Ecommerce Command: 1 - Search
  Request Params:  N_Region&S_Segment&S_Keyword1+S_Keyword2...
  Response:	   N_Status
		   N_Results Lines:
	   	   S_Segment&S_Title&S_URL&S_Overview

  Script Mapping:  Search
  DB Op:	   Select
*/
int Search ( char *Arg[], int ArgCnt, char *MsgTxt) {

    int region;
    char *ptr;
    char *lptr;
    int i;
    int listcount;
    int len=0;
    int num_of_finds;
    char Segment[SHORTNAMELEN+1];
    char Title[TITLELEN+1];
    char Url[URLLEN+1];
    char Overview[OVERVIEWLEN+1];

    ptr = MsgTxt;
    region = atoi (Arg[0]);
    
    for (listcount=1, lptr = Arg[2]; *lptr != '\0'; lptr++) {
		if ('+' == *lptr) listcount++;
    }
	if (listcount > 10)	{
		listcount = 10;
	} else {
		listcount = 10 - listcount; /* more keywords, less results */
	}
    num_of_finds = gEConstants->Scaled_Load +listcount;
		 
    ptr +=  sprintf(ptr,OK_STATUS);
    for (i = 0; i < num_of_finds; i++) {
      len += randomtext (Segment, SHORTNAMELEN-2, SHORTNAME);
      len += randomtext (Title, TITLELEN, TITLE);
      len += randomtext (Url, URLLEN, URL);
      len += randomtext (Overview, OVERVIEWLEN, OVERVIEW);
      ptr += sprintf(ptr,"%s%02d&%s&%s&%s\n", Segment, i+region, Title, Url, Overview);
      len = 0;
    }

    return 0;
}


/*
  Ecommerce Command: 2 - ProductLines
  Request Params:
		N_Region&S_CustType

  Response:	N_Status
		N_NumberOfProductLines (scales with N_Load):
		S_ProductLine1
		S_ProductLine2...
		
  Script Mapping:  browse
  DB Op:	   Select
		
*/

int ProductLines ( char *Arg[], int ArgCnt, char *MsgTxt) {
    int region;
    char *ptr;
    int i;
    int len;
    int num_of_pls;
    char ProductLine[SHORTNAMELEN+1];

    ptr = MsgTxt;
    region = atoi (Arg[0]);
    
    num_of_pls = gEConstants->Scaled_Load;
		 
    ptr +=  sprintf(ptr,OK_STATUS);
    for (i = 0; i < num_of_pls; i++) {
      len = 0;
      len += randomtext (ProductLine, SHORTNAMELEN-2, SHORTNAME);
      ptr += sprintf(ptr,"%s%02d\n", ProductLine, i+region);
    }
    
    return 0;
}


/*
  Ecommerce Command: 3 - ProductModels
  Request Params:	
		N_Region&S_CustType&S_ProductLine

  Response:	N_Status
		N_ProductModelLines (scales with N_Load):
		S_ProductName&\
		S_ProductHighlights&S_Feature1&\
		S_FeatureDesc1&S_Feature2&\
		S_FeatureDesc2&S_Feature3&\
		S_FeatureDesc3
  Script Mapping:     browse_productline 
  DB Op:           Select

*/
int ProductModels ( char *Arg[], int ArgCnt, char *MsgTxt) {
    int region;
    char *ptr;
    int i;
    int len;
    int num_of_pmls;
    char ProductName[SHORTNAMELEN+1];
    char Highlight[HIGHLIGHTSLEN+1];
    char Feature[FEATURELEN+1];
    char FeatureDesc[HIGHLIGHTSLEN+1];

    ptr = MsgTxt;
    region = atoi (Arg[0]);
    
    num_of_pmls = gEConstants->Scaled_Load ;
		 
    ptr +=  sprintf(ptr,OK_STATUS);
    for (i = 0; i < num_of_pmls; i++) {
      len = 0;
      len += randomtext (ProductName, SHORTNAMELEN-2, SHORTNAME);
      len += randomtext (Highlight, HIGHLIGHTSLEN, HIGHLIGHTS);
      len += randomtext (Feature, FEATURELEN, FEATURE);
      len += randomtext (FeatureDesc, HIGHLIGHTSLEN, HIGHLIGHTS);
      ptr += sprintf(ptr,"%s%02d&%s&%s&%s&", 
		ProductName, i+region, Highlight, Feature, FeatureDesc);
      len += randomtext (Feature, FEATURELEN, FEATURE);
      len += randomtext (FeatureDesc, HIGHLIGHTSLEN, HIGHLIGHTS);
      ptr += sprintf(ptr,"%s&%s&", Feature, FeatureDesc);
      len += randomtext (Feature, FEATURELEN, FEATURE);
      len += randomtext (FeatureDesc, HIGHLIGHTSLEN, HIGHLIGHTS);
      ptr += sprintf(ptr,"%s&%s\n", Feature, FeatureDesc);
    }
    
    return 0;
}



/*
  Ecommerce Command: 4 - ProductDetails
  Request Params:	
		N_Region&S_CustType&S_Product

  Response:	N_Status
		N_ProductDetail Lines:
		S_ProductDetail1&\
		S_ProductDetail2...
		
  Script Mapping:  productdetail
  DB Op:           Select
*/
int ProductDetails ( char *Arg[], int ArgCnt, char *MsgTxt) {
    int region;
    char *ptr;
    int i;
    int len;
    int num_of_pds;
    char ProductDetail[OVERVIEWLEN+1];

    ptr = MsgTxt;
    region = atoi (Arg[0]);
    
    num_of_pds = gEConstants->Scaled_Load * 2;
		 
    ptr +=  sprintf(ptr,OK_STATUS);
    for (i = 0; i < num_of_pds; i++) {
      len = 0;
      len += randomtext (ProductDetail, OVERVIEWLEN-2, OVERVIEW);
//      ptr += sprintf(ptr,"(%1d) %s SL=%d num_of_pds=%d\n", i+region, ProductDetail, gEConstants->Scaled_Load, num_of_pds);
      ptr += sprintf(ptr,"(%1d) %s\n", i+region, ProductDetail);
    }
    
    return 0;
}



/*
  Ecommerce Command: 5 - CustomizationChoices
  Request Params:	
		N_Region&S_CustType&S_ProductModel &N_Stage

  Response:	N_Status
		N_NumberOfComponents Lines (varies with N_Stage):
		S_ComponentType&\
		S_ComponentName&\
		S_ComponentID&\
                D_Price

  Script Mapping:  customize
  DB Op:           Select
		
*/
int CustomizationChoices ( char *Arg[], int ArgCnt, char *MsgTxt) {
    int region;
    char *ptr;
    int i;
    int len;
    int num_of_custom;
    int num_of_dlrs;
    int num_of_cnts;
    int stage;
    char ComponentType[SHORTNAMELEN+1];
    char ComponentName[LONGNAMELEN+1];
    char ComponentID[COMPONENTIDLEN+1];
    char Currency[CURRENCYLEN+1];

    ptr = MsgTxt;
    region = atoi (Arg[0]);
    stage = atoi (Arg[3]);
    
    num_of_custom = (4-stage) * 5;
    num_of_dlrs = gEConstants->Scaled_Load + region;
    num_of_cnts = (stage + region)%100;
		 
    ptr +=  sprintf(ptr,OK_STATUS);
    for (i = 0; i < num_of_custom; i++) {
      len = 0;
      if (i % 5 == 0) {
            len += randomtext (ComponentType, SHORTNAMELEN-2, SHORTNAME);
      }
      len += randomtext (ComponentName, LONGNAMELEN-2, LONGNAME);
      len += randomtext (ComponentID, COMPONENTIDLEN-2, COMPONENTID);
      len += randomtext(Currency, -(region), CURRENCIES);
      ptr += sprintf(ptr,"%s&%s&%s%01d&%d.%02d&%s\n",
 		ComponentType, ComponentName, ComponentID, i, 
		(num_of_dlrs * (i+1)), num_of_cnts, Currency);
    }
    
    return 0;
}



/*
  Ecommerce Command: 6 - GetPrice
  Request Params:	
		N_Region&S_Item&S_ComponentID1&S_ComponentID2...

  Response:	N_Status
		D_Price
		S_Currency

  Script Mapping:  cart & customize
  DB Op:           Select

*/
int GetPrice ( char *Arg[], int ArgCnt, char *MsgTxt) {
    char *ptr;
    int listcount;
    int region;
    int num_of_dlrs;
    int num_of_cnts;
    int len;
    char Currency[CURRENCYLEN+1];

    region = atoi (Arg[0]);
    listcount = ArgCnt - 2;
    num_of_dlrs = gEConstants->Scaled_Load * 10 + listcount;
    num_of_cnts = region%100;
    len = randomtext(Currency, -(region), CURRENCIES);


    ptr = MsgTxt;
    ptr += sprintf(ptr,OK_STATUS);
    ptr += sprintf(ptr,"%d.%02d\n", 
		num_of_dlrs, num_of_cnts);
    ptr += sprintf(ptr,"%s\n", Currency);
    
    return 0;
}



/*
  Ecommerce Command: 7 - CheckoutLogin
  Request Params:	
		N_Region&S_UserID

  Response:	N_Status
		S_Password

  Script Mapping:  login
  DB Op:           Select

*/
int CheckoutLogin ( char *Arg[], int ArgCnt, char *MsgTxt) {

    MD5_CTX context;
    unsigned char digest[16];
    int i;
    char *ptr;

/*  This code assumes the user's passwd is the same as the email address and
    returns the MD5 for that string
*/

    ptr = MsgTxt;
    MD5Init(&context);
    MD5Update(&context, (unsigned char *)Arg[1], strlen(Arg[1]));
    MD5Final(digest, &context);
    ptr +=  sprintf(ptr,OK_STATUS);
    for(i=0; i<16; i++)
      ptr +=  sprintf(ptr,"%02x", digest[i]);
    sprintf(ptr,"\n");
/*    sprintf(ptr,"Debug: Arg1=%s, strlen Arg1=%d\n",
      (unsigned char *)Arg[1], strlen(Arg[1]));*/

    return 0;
}



/*
  Ecommerce Command: 8 - CheckoutRegister
  Request Params:	
		N_Region&S_FirstName&S_LastName&S_Email&S_Password

  Response:	N_Status
		N_Confirmation

  Script Mapping:  login
  DB Op:           Insert

*/
int CheckoutRegister ( char *Arg[], int ArgCnt, char *MsgTxt) {
    char *ptr;
    int region;
    int confirmation;

    region = atoi(Arg[0]);
    confirmation = gEConstants->Time - gEConstants->Load - region;

    ptr = MsgTxt;
    ptr +=  sprintf(ptr,OK_STATUS);
    sprintf(ptr, "%d\n",confirmation);
    
    return 0;
}



/*
  Ecommerce Command: 9 - SaveCart
  Request Params:	
		N_Region&S_Email&\
		N_Items&\
		S_Item1&\
		N_Quantity1&\
		N_Components&\
		S_ComponentID1&\
		S_ComponentID2&\
		S_Item2&\
		N_Quantity2&\
		N_Components&\
		S_ComponentID1&\
		S_ComponentID2...

  Response:	N_Status
		N_Confirmation

  Script Mapping:  cart
  DB Op:           Insert

*/
int SaveCart ( char *Arg[], int ArgCnt, char *MsgTxt) {
    char *ptr;
    int region;
    int items;
    int quantity;
    int components;
    int index;
    int i;

    region = atoi(Arg[0]);

    ptr = MsgTxt;
    ptr += sprintf(ptr,OK_STATUS);
    ptr += sprintf(ptr, "%d%d",ArgCnt,region);

    items = atoi(Arg[2]);
    ptr += sprintf(ptr, "%d",items);
    quantity = 0;
    components = 0;
    index = 1;
    for (i = 1; i <= items; i++) {
	index = index + 3 + components;
	if (index < 0 || index >= ArgCnt)
	    break; /*Confirmation# will be incorrect */
        quantity = atoi(Arg[index]);
        components = atoi(Arg[index + 1]);
        ptr += sprintf(ptr, "%d%d",quantity, components);
    }

    ptr += sprintf(ptr, "\n");
    
    return 0;
}



/*
  Ecommerce Command: 10 - SubmitOrder
  Request Params:	
		N_Region&S_Email&S_ShipFirstName&\
		S_ShipLastName&S_ShipAddress&\
		S_ShipCity&S_ShipState&N_ShipZip&\
		S_ShipPhone&S_ShipMethod&\
		S_BillFirstName&S_BillLastName&\
		S_BillAddress&S_BillCity&S_BillState&\
		N_BillZip&S_BillPhone&S_CardType&\
		N_CardNo&N_ExpMo&N_ExpYr\
		&N_Items&\
		&S_Item1&N_Quantity1&\
		N_Components&\
		S_ComponentIDn&\
		S_Item2&N_Quantity2&\
		N_Components&\
		S_ComponentIDn.

  Response:	N_Status
		S_ShipDate&N_Confirmation

  Script Mapping:  confirm
  DB Op:           Insert


*/
int SubmitOrder ( char *Arg[], int ArgCnt, char *MsgTxt) {
    char *ptr;
    int region;
    int items;
    int quantity;
    int components;
    int index;
    int i;


    region = atoi(Arg[0]);

    ptr = MsgTxt;
    ptr += sprintf(ptr,OK_STATUS);
    ptr += sprintf(ptr, "%s&",gEConstants->ResetDate);
    ptr += sprintf(ptr, "%d%d",ArgCnt,region);

    items = atoi(Arg[21]);
    ptr += sprintf(ptr, "%d",items);
    quantity = 0;
    components = 0;
    index = 20;
    for (i = 1; i <= items; i++) {
        index = index + 3 + components;
	if (index < 0 || index >= ArgCnt)
	    break; /*Confirmation# will be incorrect */
        quantity = atoi(Arg[index]);
        components = atoi(Arg[index + 1]);
        ptr += sprintf(ptr, "%d%d",quantity, components);
    }

    ptr += sprintf(ptr, "\n");

    
    return 0;
}

/*
  Ecommerce Command: 11 - GetRegions
  Request Params:	
		N_Region_flag  	
			(where 0 or any value returns list of all regions)

  Response:	N_Status
		N_NumberRegions Lines:
		S_Region

  Script Mapping:  cart & customize
  DB Op:           Select

*/
int GetRegions ( char *Arg[], int ArgCnt, char *MsgTxt) {
    char *ptr;
    int i;
    int len = 0;
    char Currency[CURRENCYLEN+1];

    ptr = MsgTxt;

    ptr +=  sprintf(ptr,OK_STATUS);
    for (i = gEConstants->Min_region; i <= gEConstants->Max_region ; i++) {
      len = randomtext(Currency, -(i), CURRENCIES);
      ptr += sprintf(ptr,"%s%02d\n", Currency, i);
      len = 0;
    }

    return 0;
}


int Ecommerce_Undef (char* Arg[], int ArgCnt, char* MsgTxt){
char *ptr;

    ptr = MsgTxt;
    ptr +=  sprintf(ptr,ERR1_STATUS);
    strncpy(MsgTxt, ERROR_UNDEF_CMD, sizeof(ERROR_UNDEF_CMD));
    return 0;
}
 
