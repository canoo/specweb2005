/** Copyright (c) 2004-2005 Standard Performance Evaluation Corporation (SPEC)
 *               All rights reserved.
 */

/*

    SPECweb2005 Backend Simulator (BESIM)  Banking Command Module

*/

#include "besim_banking.h"


void DoBankingCommand(char* Arg[], int ArgCnt, char* MsgTxt){
  int Command;
  int Status;
  int AdjArgCnt;
  int fd;
  int s;
  char *ptr;
  struct stat FStats;

  /* Get Command Code and Adjusted Argument Count */

  Command = atoi(Arg[1]);
  AdjArgCnt = ArgCnt - 1;
  ptr = MsgTxt;

  /* If ptr to globals have not been initialized by Reset command, then try
  /* to access them from the besim globals file via mmap/read.  This helps
  /* ensure that if the besim deamon dies and restarts or there are multiples
  /* that all the initialization values are available
  */
  if ((Command != 0) && ((gBConstants == NULL) ||
      ((stat(BESIM_BANKING_GLOBALS_PATH, &FStats) != -1) &&
       (FStats.st_mtime > gBConstants->lastResetTime)))) {

#ifndef WIN32
        fd = open( BESIM_BANKING_GLOBALS_PATH, O_RDWR, 0666 );
#else
        fd = _open( BESIM_BANKING_GLOBALS_PATH, O_RDWR, 0666 );
#endif
        if ( fd >= 0 ) {
            errno = 0;
#ifdef USE_MMAP
            gBConstants = mmap(NULL, sizeof(workload_globals_t),
              (PROT_WRITE|PROT_READ), (MAP_FILE|MAP_SHARED), fd, (off_t) 0 );
            if (gBConstants == MAP_FAILED) {
                ptr += sprintf(ptr, ERR1_STATUS);
                ptr += sprintf(ptr, "mmap: %d: %s\n", errno, strerror(errno));
                gBConstants = NULL;
#ifndef WIN32
	        close(fd);
#else
	        _close(fd);
#endif
                return;
            }
            if ((gBConstants->lastResetTime == 0) ||
                 (FStats.st_mtime > gBConstants->lastResetTime))
                        gBConstants->lastResetTime = FStats.st_mtime;
#else
            gBConstants = &Constants;
            s = read(fd, (void *) gBConstants, sizeof(workload_globals_t));
            if (s != sizeof(workload_globals_t)) {
                ptr += sprintf(ptr, ERR1_STATUS);
                ptr += sprintf(ptr,"read: %d: %s\n", errno, strerror(errno));
                gBConstants = NULL;
#ifndef WIN32
	        close(fd);
#else
	        _close(fd);
#endif
                return;
            }
            gBConstants->lastResetTime = FStats.st_mtime;
#endif
        } else {
            ptr += sprintf(ptr, ERR1_STATUS);
            strncpy(ptr, ERROR_BAD_GLOBALS, sizeof(ERROR_BAD_GLOBALS));
            return;
        }
#ifndef WIN32
	close(fd);
#else
	_close(fd);
#endif
  }

  /* Validate: Command Code, Argument Cound and 1st Argument for Command */
  /* Invoke Routine to process the specified command                     */

  if (Command >= 0 && Command <  (sizeof(banking_cmdFp)/sizeof(banking_cmdFp[0]))) {
   if (AdjArgCnt == banking_cmdArgCnt[Command])
     Status = Banking_Valid1stArg(Command, Arg[2], MsgTxt)?
        (*banking_cmdFp[Command])(&Arg[2], AdjArgCnt, MsgTxt): -1;
   else {
     ptr += sprintf(ptr, ERR1_STATUS);
     strncpy(ptr, ERROR_BAD_ARGS, sizeof(ERROR_BAD_ARGS));
   }
  }
  else {
    ptr += sprintf(ptr, ERR1_STATUS);
    strncpy(ptr, ERROR_UNDEF_CMD, sizeof(ERROR_UNDEF_CMD));
  }


}




int Banking_Valid1stArg(int cmd, char* arg1, char* msg) {
    int firstarg;
    char *ptr;

    ptr = msg;

    /* Validate 1st argument for Banking Commands, All commands */
    /* except Reset(0) should have a valid user_id              */
    firstarg = atoi(arg1);
    if (cmd == 0 && firstarg != 0 ) return 1;
    if (VALID(firstarg,gBConstants->Min_userid,gBConstants->Max_userid)) return 1;
    else {
      ptr += sprintf(ptr, ERR1_STATUS);
      ptr += sprintf(ptr, "firstarg = %d, Min_userid = %d, Max_userid = %d\n",
		 firstarg,gBConstants->Min_userid,gBConstants->Max_userid);
      strncpy(ptr, ERROR_BAD_ARG1, sizeof(ERROR_BAD_ARG1));
    }
    return 0;
}


/*
  Banking Command: 0 - Reset
  Request Params:  N_TIME&N_MinUserID&N_MaxUserID&Load&Check_URL_Base
                    &Num_check_subdirs
  Response:		    S_Status (DONE)
  Script Mapping:  Prime Client Only
  DB Op:		    NA
*/

int Banking_Reset (char* Arg[], int ArgCnt, char* MsgTxt){

    int len = 0;
    time_t now;
    const struct tm *tmstruct;
    char *ptr;
    int fd;
    struct stat FStats;

    ptr = MsgTxt;

    /* Get YYYYMMDD and return to Prime Client */
    /* May be used for Date fields in response and/or validation */

    (void)setlocale(LC_ALL, "");
    time(&now);
    tmstruct = localtime(&now);
    strftime(Constants.ResetDate, 16, "%Y%m%d", tmstruct);

    /* Save values for use in subsequent commands */
    Constants.Time = atoi(Arg[0]);
    Constants.Min_userid = atoi(Arg[1]);
    Constants.Max_userid = atoi(Arg[2]);
    Constants.Load = atoi(Arg[3]);
    Constants.uid_range = Constants.Max_userid - Constants.Min_userid + 1 ;
    Constants.gflag = 9;
    Constants.lastResetTime = 0;

    len = (strlen(Arg[4]) < CUBLEN)?
		 (strlen(Arg[4])):CUBLEN-1;
    strncpy(Constants.Check_URL_Base, Arg[4], len + 1);
	Constants.Num_check_subdirs = atoi(Arg[5]);

    /* save the globals to file for use by other Besim deamons */
    unlink (BESIM_BANKING_GLOBALS_PATH);
#ifndef WIN32
    fd = open( BESIM_BANKING_GLOBALS_PATH, O_RDWR|O_CREAT, 0666 );
#else
    fd = _open( BESIM_BANKING_GLOBALS_PATH, O_RDWR|O_CREAT, 0666 );
#endif
    if ( fd >= 0 ) {
        write (fd, &Constants, sizeof(workload_globals_t));
        fsync(fd);
        gBConstants = &Constants;
        if (-1 == stat( BESIM_BANKING_GLOBALS_PATH, &FStats) ) {
                ptr += sprintf(ptr, ERR1_STATUS);
                strncpy(ptr, ERROR_BAD_GLOBALS, sizeof(ERROR_BAD_GLOBALS));
#ifndef WIN32
	        close(fd);
#else
	        _close(fd);
#endif
                return 0;
        } else {
                lseek (fd, 0, SEEK_SET);
                gBConstants->lastResetTime = FStats.st_mtime;
                write (fd, &Constants, sizeof(workload_globals_t));
                fsync(fd);
#ifndef WIN32
                sleep (1);
#else
                Sleep (1000);
#endif

        }
#ifndef WIN32
	close(fd);
#else
	_close(fd);
#endif

    } else {
                 ptr += sprintf(ptr, ERR1_STATUS);
                 strncpy(ptr, ERROR_BAD_GLOBALS, sizeof(ERROR_BAD_GLOBALS));
                 return 0;
    }

    ptr += sprintf(ptr, OK_STATUS);
    sprintf(ptr,
      "DONE ResetDate = %s, Time=%d,Min_UID=%d,Max_UID=%d,Load=%d,ChkBase=%s,Subdirs=%d\n",
      gBConstants->ResetDate, gBConstants->Time, gBConstants->Min_userid,
      gBConstants->Max_userid, gBConstants->Load, gBConstants->Check_URL_Base,
	  gBConstants->Num_check_subdirs);

    return 0;
}



/*
  Banking Command: 1 - LoginPasswd
  Request Params:  N_UserID
  Response:		    N_Password
  Script Mapping:  Login
  DB Op:		    Select
*/

int LoginPasswd (char* Arg[], int ArgCnt, char* MsgTxt){

    MD5_CTX context;
    unsigned char digest[16];

    int i;
    char *ptr;

    ptr = MsgTxt;

/*  This code assumes the user's passwd is the same as the USER_ID and
    returns the MD5 for the 10 characters in that numeric string
*/

    MD5Init(&context);
    MD5Update(&context, (unsigned char *)Arg[0], strlen(Arg[0]));
    MD5Final(digest, &context);

    ptr +=  sprintf(ptr,OK_STATUS);
    for(i=0; i<16; i++)
      ptr +=  sprintf(ptr,"%02x", digest[i]);
    sprintf(ptr,"\n");
    /*sprintf(ptr,"Debug: Arg0=%s, strlen Arg0=%d\n",
      (unsigned char *)Arg[0], strlen(Arg[0]));*/


    return 0;

}




/*
  Banking Command: 2 - AcctBalances
  Request Params:  N_UserID
  Response:		    N_NumberOfAccts Lines: N_Acct_#&N_Type&D_Balance
  Script Mapping:  Login, Order_check, Transfer
  DB Op:		    Select
*/

int AcctBalances (char* Arg[], int ArgCnt, char* MsgTxt){

    acct_info Accts[10];
    int i;
    int uid;
    int num_of_accts;
    char *ptr;

    ptr = MsgTxt;

    uid = atoi(Arg[0]);
    num_of_accts = (uid % 10) +1;


    ptr +=  sprintf(ptr,OK_STATUS);
    for (i = 0; i < num_of_accts; i++) {
      Accts[i].acct_num = (uid + gBConstants->uid_range + i);
      Accts[i].acct_type = (gBConstants->Time % 10) + i;
      Accts[i].dollars = (gBConstants->Time % 10000) + (uid % (10 + i)) ;
      Accts[i].cents = ((uid+i) % 100);

      ptr += sprintf(ptr, "%010d&%02d&%d.%02d\n", Accts[i].acct_num,
		 Accts[i].acct_type, Accts[i].dollars, Accts[i].cents);
    }
    return 0;
}



/*
  Banking Command: 3 -AcctSummary
  Request Params:  N_UserID
  Response:		    N_NumberOfAcctsLines: N_Acct_#&N_Type&D_Balance&\
		 		    D_TotalDeposit&D_AvgDeposit&D_TotalWitdrawal&D_AvgWithdrawal
  Script Mapping:  Account_summary
  DB Op:		    Select


*/

int AcctSummary (char* Arg[], int ArgCnt, char* MsgTxt) {

    acct_info Accts[10];
    int i;
    int uid;
    int num_of_accts;
    char *ptr;

    ptr = MsgTxt;

    uid = atoi(Arg[0]);
    num_of_accts = (uid % 10) +1;

    ptr +=  sprintf(ptr,OK_STATUS);
    for (i = 0; i < num_of_accts; i++) {
      Accts[i].acct_num = (uid + gBConstants->uid_range + i);
      Accts[i].acct_type = (gBConstants->Time % 10) + i;
      Accts[i].dollars = (gBConstants->Time % 10000) + (uid % (10 + i)) ;
      Accts[i].cents = (uid % 100);
      Accts[i].ttl_dep_dolrs = (Accts[i].dollars % 100) + (i * 100);
      Accts[i].ttl_dep_cents =  Accts[i].cents;
      Accts[i].avg_dep_dolrs = Accts[i].dollars % 100;
      Accts[i].avg_dep_cents = Accts[i].cents;
      Accts[i].ttl_wthdrw_dolrs = Accts[i].dollars % 99 + (i * 50);
      Accts[i].ttl_wthdrw_cents = Accts[i].cents;
      Accts[i].avg_wthdrw_dolrs = Accts[i].dollars % 99;
      Accts[i].avg_wthdrw_cents = Accts[i].cents;

      ptr += sprintf(ptr,
        "%010d&%02d&%d.%02d&%d.%02d&%d.%02d&%d.%02d&%d.%02d\n",
        Accts[i].acct_num, Accts[i].acct_type,
        Accts[i].dollars, Accts[i].cents,
        Accts[i].ttl_dep_dolrs, Accts[i].ttl_dep_cents,
        Accts[i].avg_dep_dolrs, Accts[i].avg_dep_cents,
        Accts[i].ttl_wthdrw_dolrs, Accts[i].ttl_wthdrw_cents,
        Accts[i].avg_wthdrw_dolrs, Accts[i].avg_wthdrw_cents);
    }
    return 0;

}



/*
  Banking Command: 4 - CheckDetail1
  Request Params:  N_UserID&N_Check
  Response:		    N_Acct&S_FrontURL&S_BackURL
  Script Mapping:  Check_detail_html, Check_detail_image
  DB Op:		    Select

  Note: URL's for check images are created using the base URL path info
  supplied with the Reset (0) Command and appending:
  "/user<10char_UID>/<CIF|CIB><6digit_Check_Number>"
  (where CIF stands for check image front and CIB stands for check image back)
*/

int CheckDetail (char* Arg[], int ArgCnt, char* MsgTxt){

    int uid;
    int check_number;
    int acct_num;
    char *ptr;
    char CheckFront[CUBLEN];
    char CheckBack[CUBLEN];

    ptr = MsgTxt;

    uid = atoi(Arg[0]);
    check_number = atoi(Arg[1]);

    acct_num = uid + gBConstants->uid_range;
	/* add support for subdirectories -- GWK */
	if (gBConstants->Num_check_subdirs > 0) {
		char tmpBuf[CUBLEN];
		char buffer[10];
		int subdirLen;
		int baseStrLen = strlen(gBConstants->Check_URL_Base);
		int subdirNum = uid%gBConstants->Num_check_subdirs;
		if (subdirNum == 0) subdirNum = gBConstants->Num_check_subdirs;
		subdirLen = sprintf(buffer, "%d", subdirNum);
		strncpy(tmpBuf,gBConstants->Check_URL_Base,baseStrLen-subdirLen);
                /* null terminate char string */
		tmpBuf[baseStrLen-subdirLen] = 0;
		strcat(tmpBuf, buffer);
		sprintf(CheckFront, "%s/user%010d/CIF%06d",
			tmpBuf, uid, check_number);
		sprintf(CheckBack, "%s/user%010d/CIB%06d",
			tmpBuf, uid, check_number);
	}
	else {
		sprintf(CheckFront, "%s/user%010d/CIF%06d",
			gBConstants->Check_URL_Base, uid, check_number);
		sprintf(CheckBack, "%s/user%010d/CIB%06d",
			gBConstants->Check_URL_Base, uid, check_number);
	}
    ptr +=  sprintf(ptr,OK_STATUS);
    sprintf(ptr, "%010d&%s&%s\n", acct_num, CheckFront, CheckBack);
    return 0;

}



/*
  Banking Command: 5 - Bill Payment
  Request Params:  N_UserID
  Response:		    N_NumberOfPayees Lines: N_PayeeId&D_Payment&N_Date
  Script Mapping:  Bill_pay
  DB Op:		    Select
*/

int BillPayment (char* Arg[], int ArgCnt, char* MsgTxt){

    int i;
    int uid;
    int bills;
    char *ptr;

    ptr = MsgTxt;
    uid = atoi(Arg[0]);

    bills = uid%10 + 1;
    ptr +=  sprintf(ptr,OK_STATUS);
    for (i = 0; i < bills; i++) {
      ptr += sprintf(ptr, "%s&%s&%s\n",PayeesId[i], Payments[i],
		 gBConstants->ResetDate);
    }
    return 0;
}



/*
  Banking Command: 6 - PostPayee
  Request Params:
		N_UserID&N_PayeeID&S_Name&S_Address&S_City&S_State&N_Zip&S_Phone
  Response:		    S_Status (DONE)
                            N_Confirmation
  Script Mapping:  Post_payee
  DB Op:		    Insert
*/

int PostPayee (char* Arg[], int ArgCnt, char* MsgTxt){

    int uid;
    int confirmation;
    char *ptr;

    ptr = MsgTxt;
    uid = atoi(Arg[0]);
    confirmation = atoi(Arg[6]) + uid + gBConstants->Time - gBConstants->Load;
    ptr +=  sprintf(ptr,OK_STATUS);
    sprintf(ptr, "%d\n", confirmation);
    return 0;

}



/*
  Banking Command: 7 - QuickPay
  Request Params:  N_UserID&N_PayeeId&N_Date&D_Amount
  Response:		    S_Status (DONE)
                            N_Confirmation
  Script Mapping:  Quick_pay
  DB Op:		    Insert
*/

int QuickPay (char* Arg[], int ArgCnt, char* MsgTxt){

    int uid;
    int confirmation;
    char *ptr;

    ptr = MsgTxt;
    uid = atoi(Arg[0]);
    confirmation = atoi(Arg[3]) + uid + gBConstants->Time - gBConstants->Load;

    ptr +=  sprintf(ptr,OK_STATUS);
    sprintf(ptr, "%d\n", confirmation);

    return 0;


}



/*
  Banking Command: 8 - ReviewBillPay
  Request Params:  N_UserID&N_StartDate&N_EndDate
  Response:		    N_NumberOfPayees Lines: N_PayeeId&N_Date&D_Amount
  Script Mapping:  Bill_pay_status_output
  DB Op:		    Select
*/

int ReviewBillPay (char* Arg[], int ArgCnt, char* MsgTxt){

    int i;
    int uid;
    char *ptr;
    int startdate, enddate;
    int bills;

    ptr = MsgTxt;
    uid = atoi(Arg[0]);
    startdate =  atoi(Arg[1]);
    enddate =  atoi(Arg[2]);
    bills = uid%10 + 1;
    ptr +=  sprintf(ptr,OK_STATUS);
    for (i=0; i < bills; i++) {
      ptr += sprintf(ptr, "%s&%s&%s\n",PayeesId[i], Arg[1], Payments[i]);
    }
    return 0;
}



/*
  Banking Command: 9 - Profile
  Request Params:  N_UserID
  Response:		    S_Address&S_Email&S_Phone
  Script Mapping:  Profile
  DB Op:		    Select
*/

int Profile (char* Arg[], int ArgCnt, char* MsgTxt){

    int i;
    int uid;
    int streetnum;
    char *ptr;

    ptr = MsgTxt;
    uid = atoi(Arg[0]);
    i = uid%10;
    streetnum = (100+uid)% 1000;

    ptr +=  sprintf(ptr,OK_STATUS);
    sprintf (ptr, "%d %s&%s&%s&%s&%s@isp.net&%s\n",
        streetnum, Street[i], City[i], State[i], Zip[i], Arg[0], Phone[i]);

    return 0;

}



/*
  Banking Command: 10 - ChangeProfile
  Request Params:  N_UserID& S_Address&S_Email&S_Phone
  Response:		    S_Status (DONE)
                            N_Confirmation
  Script Mapping:  Change_profile
  DB Op:		    Update
*/

int ChangeProfile (char* Arg[], int ArgCnt, char* MsgTxt){

    int uid;
    int confirmation;
    char *ptr;
    strncpy(MsgTxt, DOBANKING, sizeof(DOBANKING));

    ptr = MsgTxt;
    uid = atoi(Arg[0]);
    confirmation = uid  + gBConstants->Time - gBConstants->Load;

    ptr +=  sprintf(ptr,OK_STATUS);
    sprintf(ptr, "%d\n", confirmation);
    return 0;

}



/*
  Banking Command: 11 - PlaceChkOrder
  Request Params:  N_UserID&N_Acct#&N_Date&D_Price
  Response:		    S_Status (DONE)
                            N_Confirmation
  Script Mapping:  Place_check_order
  DB Op:		    Select,Update,Insert
*/

int PlaceChkOrder (char* Arg[], int ArgCnt, char* MsgTxt){
    int uid;
    char *ptr;
    double price;
    int acct;
    unsigned int confirmation;

    strncpy(MsgTxt, DOBANKING, sizeof(DOBANKING));

    ptr = MsgTxt;
    uid = atoi(Arg[0]);
    acct = atoi(Arg[1]);
    price = atof(Arg[3]);

    confirmation = acct + uid + gBConstants->Time - gBConstants->Load;

    ptr +=  sprintf(ptr,OK_STATUS);
    sprintf(ptr, "%u\n",confirmation);
    return 0;

}



/*
  Banking Command: 12  - PostTransfer
  Request Params:  N_UserID&N_Acct1&D_Amount&N_Acct2,N_Date
  Response:		    N_NumberOfAccts Linesi(2): N_Acct_# &D_Balance
  Script Mapping:  Post_transfer
  DB Op:		    Select,Update,Insert,Insert,Select
*/

int PostTransfer (char* Arg[], int ArgCnt, char* MsgTxt){
    int uid;
    char *ptr;
    double xferamt, balance1, balance2;
    int acct1, acct2, date;

    ptr = MsgTxt;
    uid = atoi(Arg[0]);
    acct1 = atoi(Arg[1]);
    xferamt = atof(Arg[2]);
    acct2 = atoi(Arg[3]);
    date = atoi(Arg[4]);
    balance1 = (gBConstants->Time%1000) + (uid%10000) - xferamt;
    balance2 = (gBConstants->Time%1000) + xferamt;
    ptr +=  sprintf(ptr,OK_STATUS);
    ptr += sprintf(ptr, "%010d&%-10.2f\n",acct1,balance1);
    ptr += sprintf(ptr, "%010d&%-10.2f\n",acct2,balance2);
    return 0;

}



int Banking_Undef (char* Arg[], int ArgCnt, char* MsgTxt){
char *ptr;

    ptr = MsgTxt;
    ptr +=  sprintf(ptr,ERR1_STATUS);
    strncpy(MsgTxt, ERROR_UNDEF_CMD, sizeof(ERROR_UNDEF_CMD));
    return 0;
}
