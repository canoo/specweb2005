/** Copyright (c) 2004-2005 Standard Performance Evaluation Corporation (SPEC)
 *               All rights reserved.
 */

/*

    SPECweb2005 Backend Simulator (BESIM)  

	besim_common.h

*/
#if !defined(BESIM_COMMON_H_INCLUDED)

#define BESIM_COMMON_H_INCLUDED

#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <ctype.h>
#include <errno.h>
#include <string.h>
#include <time.h>
#include <locale.h>
#include <limits.h>
#ifndef WIN32
#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#else
#include <windows.h>
#include <io.h>
#include <sys/types.h>
#include <sys/stat.h>
#define stat _stat
#define fsync _commit
#endif

#ifndef DEFAULT_TOP_DIR
#define DEFAULT_TOP_DIR "/www"
#endif


#define BOILERPLATE_START \
"<html>\n"\
"<head><title>SPECweb2005 BESIM</title></head>\n"\
"<body>\n"\
"<p>SERVER_SOFTWARE = %s\n"\
"<p>REMOTE_ADDR = %s\n"\
"<p>SCRIPT_NAME = %s\n"\
"<p>QUERY_STRING = %s\n"\
"<pre>\n"

#define BOILERPLATE_END "\n</pre>\n</body></html>\n"

#ifdef WIN32
#define PATH_MAX 512
#define BUFLEN 64*1024
#else
#define BUFLEN 64*1024
#endif

#define ERROR_NO_DOC_TOP "Can't find top of document tree, please configure value CGI script manually.\n"
#define  ERROR_NO_QUERY_STRING "No Query String in Request\n"
#define  ERROR_UNDEF_WORKLOAD "Undefined Workload Type in Request\n"
#define  ERROR_BAD_GLOBALS "Access to globals failed\n"

#define  OK_STATUS "0\n"
#define  ERR1_STATUS "1\n"

/* Supported Workloads */

#define BANKING 	1
#define ECOMMERCE	2
#define SUPPORT		3


/* Workload Global data structure */

#define CUBLEN 256

typedef struct _workload_globals_t {
  int lastResetTime;
  int gflag;
  int Time;
  int Min_userid;
  int Max_userid;
  int uid_range;
  int Min_region;
  int Max_region;
  int Load;
  int Scaled_Load;
  int Num_Products;
  int Num_ProductLines;
  int Num_ModelLines;
  int Num_SearchResults;
  int region_range;
  int Num_check_subdirs;
  char ResetDate[16];
  char Check_URL_Base[CUBLEN];
} workload_globals_t;


#endif /*BESIM_COMMON_H_INCLUDED*/
