/** Copyright (c) 2004-2005 Standard Performance Evaluation Corporation (SPEC)
 *               All rights reserved.
 */

/*

    SPECweb2005 Backend Simulator (BESIM)  NSAPI in C 

    Calling Sequence:

    GET /BeSim/specweb2005_besim?<Workload_Type>&<Command_Type>&\
	<param=value>[&<param=value>]


    Returns:

	HTTP 200 OK
        Cache-control: no-store
	Content-type: text/html
	Content-Length: [length of all return text - excluding headers]

	<html>
	<head><title>SPECweb2005 BESIM</title></head>
	<body>
	<p>SERVER_SOFTWARE = [ServerSoftware]
	<p>REMOTE_ADDR = [RemoteAddr]
	<p>SCRIPT_NAME = [ScriptName]
	<p>QUERY_STRING = [QueryString]
	<pre>
	[BeSIM MessageText]
	</pre>
	</body></html>

*/

#include "besim_common.h"
#include <nsapi.h>

void ProcessQuery (char *Buf, char *query_string);
int send_header(Session *sn, Request *rq, int Len);
void DoBankingCommand(char *Arg[],int ArgCnt, char* MsgText);
void DoEcommerceCommand(char *Arg[],int ArgCnt, char* MsgText);
void DoSupportCommand(char *Arg[],int ArgCnt, char* MsgText);

static char *server_software;
static int server_software_len;

static char* lastBuf = "</pre>\n</body></html>\n";
static int lastLen = 22;

NSAPI_PUBLIC int init_besim(pblock *pb, Session *sn, Request *rq) {

  server_software = system_version();
  server_software_len = strlen(server_software);

  return REQ_PROCEED;
}


NSAPI_PUBLIC int service_besim(pblock *pb, Session *sn, Request *rq) {

  struct iovec iov[2];
  int ivc;
  int firstLen;
  char *msgBuf;
  int msgLen;

  char *remote_address;
  char *script_name;
  char *query_string;
  char *request_method;


  msgBuf = MALLOC(BUFLEN);
  remote_address = pblock_findval("ip", sn->client);
  script_name = pblock_findval("uri", rq->reqpb);  
  query_string = pblock_findval( "query", rq->reqpb); 
  request_method = pblock_findval( "method", rq->reqpb);

  if (query_string != NULL && strlen(query_string) > 0) {
      firstLen = sprintf(msgBuf, BOILERPLATE_START, server_software, remote_address, script_name, query_string);
      ProcessQuery(msgBuf + firstLen, query_string);
  } else { /* no query string */
      sprintf(msgBuf, "%s%s", ERR1_STATUS, ERROR_NO_QUERY_STRING); 
  }

  msgLen = strlen(msgBuf); 

  if (send_header(sn, rq, msgLen + lastLen) == REQ_ABORTED)
      return REQ_ABORTED; 

  ivc = 0;
  iov[ivc].iov_base  = msgBuf;
  iov[ivc++].iov_len = msgLen;

  iov[ivc].iov_base = lastBuf;
  iov[ivc++].iov_len = lastLen;

  if (net_writev(sn->csd, iov, ivc) == IO_ERROR) {
    msgBuf[0] = 0;
    PR_GetErrorText(msgBuf);
    log_ereport(LOG_FAILURE, "Writev failed %d %s", PR_GetOSError(), msgBuf);
    return REQ_ABORTED;
  }

  return REQ_PROCEED;

}

void ProcessQuery (char *Buf, char *Qstring) {
  int WorkloadType;
  int CommandType;
  char *pStr;
  char *Arg[100];
  int ArgCnt;

  /* Place each ampersand separated argument into the Arg list */

  Arg[0] = Qstring;
  for (ArgCnt = 0, pStr = Qstring; *pStr != '\0'; pStr++) {
    if ('&' == *pStr) {
      *pStr = '\0';
      ArgCnt++;
      Arg[ArgCnt] = pStr + 1;
    }
    else if ( ',' == *pStr) {
      *pStr = '\0';
      ArgCnt++;
      Arg[ArgCnt] = pStr + 1;
    }
  }

  WorkloadType = atoi(Arg[0]);
  CommandType = atoi(Arg[1]);

  /* Select and process workload command */

  switch (WorkloadType) {
case BANKING: 
    DoBankingCommand(Arg,ArgCnt,Buf);
    break;
case ECOMMERCE:
    DoEcommerceCommand(Arg,ArgCnt,Buf);
    break;
case SUPPORT:
    DoSupportCommand(Arg,ArgCnt,Buf);
    break;
default:
    sprintf(Buf, "%s%s", ERR1_STATUS, ERROR_UNDEF_WORKLOAD);
    break;
  }
  
} 

int send_header(Session *sn, Request *rq, int Len) {
 
  /* Add HTTP headers to output */

  protocol_status(sn, rq, PROTOCOL_OK, NULL);

  /* Set cache-control */
   pblock_nvinsert("cache-control", "no-store", rq->srvhdrs);

  /* Set content-length */
  pblock_nninsert("content-length", Len, rq->srvhdrs);

  /* Set content-type and begin response */
  pblock_nvinsert("content-type", "text/html", rq->srvhdrs);

  /* A noaction response from this function means the request was HEAD */
  if (protocol_start_response(sn, rq) == REQ_NOACTION)
         return REQ_ABORTED; 
  
  return REQ_PROCEED;

}
