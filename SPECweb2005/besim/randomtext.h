/** Copyright (c) 2004-2005 Standard Performance Evaluation Corporation (SPEC)
 *               All rights reserved.
 */

#if !defined(RANDOMTEXT_H_INCLUDED)
#define RANDOMTEXT_H_INCLUDED

#define SHORTNAME	1
#define LONGNAME	2
#define FEATURE		3
#define COMPONENTID	4
#define TITLE		5
#define OVERVIEW	6
#define HIGHLIGHTS	7
#define URL		8
#define DESCRIPTION	9
#define CURRENCIES	10
#define CATEGORY	11
#define PRODUCT		12
#define FILENAME	13
#define FILEDESC	14
#define ADDTNLINFO	15

#define SHORTNAMELEN	 15
#define LONGNAMELEN	 31
#define FEATURELEN	 31
#define COMPONENTIDLEN	 63
#define TITLELEN	 63
#define OVERVIEWLEN	255
#define HIGHLIGHTSLEN	127
#define URLLEN		127
#define DESCRIPTIONLEN	127
#define CURRENCYLEN	  4
#define CATEGORYLEN	 31
#define FILENAMELEN	255
#define FILEDESCLEN	255
#define ADDTNLINFOLEN	2047
#define PRODUCTLEN	 51

#define PHRASELEN	128


#if !defined(BESIM_ECOMMERCE_H_INCLUDED) && !defined(BESIM_SUPPORT_H_INCLUDED)

char *Currency[20] = {"USD", "CAD", "EUR", "JPY", "AUD", 
	              "MXN", "ARS", "BMD", "BOB", "CLP",
		      "EGP", "HKD", "INR", "JMD", "NOK",
		      "RUR", "SSL", "THB", "VEB", "YUM"};

char *Article[5] = {"The", "A", "One", "Some", "This"};
char *article[5] = {"the", "a", "one", "some", "this"};
char *Superlative[10] = 
        { "Best", "Leading", "State-of-the-Art", "High-Performance", "Stylish",
	  "Cool", "Industry-Standard", "Award-Winning", "New", "Value-Priced"};
char *Adjective[10] = 
	{ "1200MHz",  "36GB",   "100Mbs",    "8x", "4.0GHz", 
          "1GB",      "Red",    "White",     "Blue",  "Open-box"};
char *adjective[10] = 
	{ "new",  "newest",   "best",    "brightest", "10x", 
          "fast", "slimline", "lowcost", "highspeed", "reliable"};
char *CatGroups[5] = 
        { "Personal", "Office", "Corporate", "Academic", "Value" };
char *Nouns[25] = 
	{ "Computers", "DIMMs",   "Processors", "Printers",    "Cartridges",
	  "Disks",     "Networks", "Cameras",   "PDAs",        "DVDs",
	  "Boxes",     "CD-ROMs",  "Laptops",   "Accessories", "Software",
	  "Servers",   "LCD-TVs",  "Monitors",  "Programs",    "Projectors",
	  "Desktops",  "Operating Systems",     "Video Centers", 
                                           "Digital Cameras", "MP3 Players"}; 
char *noun[20] = 
	{ "computer", "memory", "processor", "printer", "cartridge",
	  "disk",     "cable",  "camera",    "PDA",     "DVD",
	  "box",      "CD-ROM", "RAM",       "paper",   "software",
	  "price",    "LCD",    "monitor",   "program", "projector"}; 

char *Verb[20] =
	{ "Is",       "Has",     "Runs",     "Includes", "Meets",
	  "Works",    "Sets",    "Makes",    "Looks",    "Builds",
	  "Drives",   "Sends",   "Changes",  "Receives", "Takes",
	  "Upgrades", "Applies", "Delivers", "Features", "Adjusts"};

char *verb[20] =
	{ "is",       "has",     "runs",     "includes", "meets",
	  "works",    "sets",    "makes",    "looks",    "builds",
	  "drives",   "sends",   "changes",  "receives", "takes",
	  "upgrades", "applies", "delivers", "features", "adjusts"};

char *DirectVerb[20] = 
	{ "Click",    "Download",  "Select",   "Double-Click", "Extract",
          "Copy",     "Load",      "Save",      "Remove",       "Send",
          "Send",     "Test",      "Press",    "Set",          "Close",
          "Power-On", "Power-Off", "Shutdown", "Get",          "Install"};

char *directVerb[20] = 
	{ "click",    "download",  "Select",   "double-click", "extract",
          "copy",     "load",      "save",      "remove",       "send",
          "send",     "test",      "press",    "set",          "close",
          "power-on", "power-off", "shutdown", "get",          "install"};


char *preposition[10] =
	{ "to", "from",   "with",    "like",  "on",
	  "by", "before", "through", "under", "over" };
           
char *adverb[10] = 
	{"fast", "quickly", "now",  "today", "always",
	 "here", "too",     "fine", "great", "really" };

char *ModelType[10] =
{ "PRO", "XL-", "ES-", "SD-", "IS-", 
  "SCH", "UX-", "VAL", "EZ-", "RX-"}; 

char *Suffix[10] =
{ "exe", "bin", "zip", "class", "doc", 
  "tgz", "txt", "sh", "kit", "upd"}; 

char *SuffixDesc[10] = 
{ "executable binary", "self-installing binary", "compressed file",
  "JVM binary", "document file", 
  "compressed tar file", "ascii text file", "shell script", 
  "distibution", "auto-updater" };

char *Software[10] = 
{ "RNZX_BIOS", "RN3K_DIAGNOSTIC", "RN2045_Utilities", 
  "Xilin_V3.01", "Driver_Xilin_V3.0",
  "Patch_Set_OS743N7", "NP-OS_V13.41", "CafeOS2.3.1_HA82_Drivers"
  "CafeOS2.3.1_NA90_SecurityPatch", "FreeBinOS7.2to7.3_Updater" };


int randomtext (char* textbuf, int textlength, int texttype);
#endif

#endif /*RANDOMTEXT_H_INCLUDED*/
