/** Copyright (c) 2004-2005 Standard Performance Evaluation Corporation (SPEC)
 *               All rights reserved.
 */

/*

    SPECweb2005 Backend Simulator (BESIM)  Banking Command Module 

    besim_banking.h

*/

#if !defined(BESIM_BANKING_H_INCLUDED)

#include "besim_common.h"
#include "md5.h"

int Banking_Reset (char* Arg[], int ArgCnt, char* MsgTxt);	 
int LoginPasswd (char* Arg[], int ArgCnt, char* MsgTxt);	
int AcctBalances (char* Arg[], int ArgCnt, char* MsgTxt);	
int AcctSummary (char* Arg[], int ArgCnt, char* MsgTxt);
int CheckDetail (char* Arg[], int ArgCnt, char* MsgTxt);	 
int BillPayment (char* Arg[], int ArgCnt, char* MsgTxt);	
int PostPayee (char* Arg[], int ArgCnt, char* MsgTxt);	
int QuickPay (char* Arg[], int ArgCnt, char* MsgTxt);
int ReviewBillPay (char* Arg[], int ArgCnt, char* MsgTxt); 
int Profile (char* Arg[], int ArgCnt, char* MsgTxt);	
int ChangeProfile (char* Arg[], int ArgCnt, char* MsgTxt);	
int PlaceChkOrder (char* Arg[], int ArgCnt, char* MsgTxt);
int PostTransfer (char* Arg[], int ArgCnt, char* MsgTxt);  
int Banking_Undef (char* Arg[], int ArgCnt, char* MsgTxt);
int Banking_Valid1stArg(int cmd, char * arg1, char* msg);

#define  ERROR_UNDEF_CMD "Undefined Command Type in Request\n"
#define  ERROR_BAD_ARG1 "Invalid Argument 1 (userid) for Command Type\n"
#define  ERROR_BAD_ARGS "Invalid Arguments for Command Type\n"
#define  ERROR_BAD_ARGS2 "Invalid Arguments2 for Command Type\n"
#define  ERROR_CMD_FAILED "Command Failed\n"

#define DOBANKING "DoBankingCommand() Called\n"

  int (*banking_cmdFp[16])() = 
	{ Banking_Reset, LoginPasswd,	AcctBalances,	AcctSummary,
	  CheckDetail,	 BillPayment,	PostPayee,	QuickPay,
	  ReviewBillPay, Profile,	ChangeProfile,	PlaceChkOrder,
	  PostTransfer,  Banking_Undef,	Banking_Undef,	Banking_Undef };
			 
  int banking_cmdArgCnt[16] = 
	{ 6,	 1,	1,	1,
	  2,	 1,	8,	4,
	  3, 	 1,	7,	4,
	  5,  	 0,	0,	0 };

#define VALID(v,n,x)  (((v>=n)&&(v<=x))?(1):(0))
			 
#define CUBLEN 256

/* typedef struct _banking_globals_t {
  int lastResetTime;
  int gflag;
  int Time;
  int Min_userid;
  int Max_userid;
  int Load;
  int uid_range;
  char ResetDate[16];
  char Check_URL_Base[CUBLEN];
  int Num_check_subdirs;
} banking_globals_t; */

workload_globals_t Constants;
workload_globals_t *gBConstants = NULL;

  

typedef struct _acct_info {
  int acct_num;
  int acct_type;
  int dollars;
  int cents;
  int ttl_dep_dolrs;
  int ttl_dep_cents;
  int avg_dep_dolrs;
  int avg_dep_cents;
  int ttl_wthdrw_dolrs;
  int ttl_wthdrw_cents;
  int avg_wthdrw_dolrs;
  int avg_wthdrw_cents;
} acct_info;


    char *PayeesId[10] =
        {"Electric", "Mortgage", "CreditCard", "Oil", "Cable",
        "Insurance", "Telephone", "Wireless", "Auto", "IRS"};
    char *Payees[10] =
        {"Electric Co.", "Mortgage Inc.", "CreditCard", "Oil Co.", "Cable",
        "Insurance Inc.", "Telephone Co.", "Wireless Inc.", "Auto Inc.", "IRS"};
    char *Payments[10] =
        {"100.00", "1273.42", "20.00", "120.00", "40.00",
        "179.28", "29.00", "39.99", "207.00", "250.00"};
    char *Street[10] = 
	{"Aspen Rd", "Birch Ln", "CloudTree Ave", "Dogwood Dr", "Elm St",
		"Heath Way", "Linden Blvd", "Magnolia Ave", "Olive Cirle", "Palm way"};
    char *City[10] =
	{"Northford", "Easton", "Sudbury", "Westville", "Middletown",
        "Portmouth", "Bridgewater", "Littleton", "Wayland", "Upton"};
    char *State[10] =
	{"PA", "OH", "NJ", "MD", "DE", 
	"VA", "NC", "SC", "TN", "WV"};
    char *Zip[10] =
	{"01812", "14112", "13821", "02012", "01934",
	"01732", "01431", "03821", "05014", "01967"};
    char *Phone[10] =
	{"123-234-3456", "223-334-4456", "333-445-5678", "414-516-7271",
        "555-221-0987", "800-888-1223", "866-912-8765", "777-667-1256",
	"929-877-4929", "877-392-2855"};

    /*MD5_CTX context;*/
    /*unsigned char buffer[1024], digest[16];*/


#ifndef BESIM_BANKING_GLOBALS_PATH
#ifndef WIN32
#define BESIM_BANKING_GLOBALS_PATH "/tmp/besim_banking.globals"
#else
#define BESIM_BANKING_GLOBALS_PATH "c:\\besim_banking.globals"
#endif
#endif

#endif /*BESIM_BANKING_H_INCLUDED*/	
