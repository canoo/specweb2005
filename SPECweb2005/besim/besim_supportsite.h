/** Copyright (c) 2004-2005 Standard Performance Evaluation Corporation (SPEC)
 *               All rights reserved.
 */

/*

    SPECweb2005 Backend Simulator (BESIM)  Support Site Command Module 

    besim_supportsite.h

*/

#if !defined(BESIM_SUPPORT_H_INCLUDED)

#define BESIM_SUPPORT_H_INCLUDED

#include "besim_common.h"
#include "md5.h"

int Support_Reset (char* Arg[], int ArgCnt, char* MsgTxt);	 
int CategoryListing (char* Arg[], int ArgCnt, char* MsgTxt);	
int ProductListing (char* Arg[], int ArgCnt, char* MsgTxt);
int SearchProducts (char* Arg[], int ArgCnt, char* MsgTxt);	
int DownloadCategories (char* Arg[], int ArgCnt, char* MsgTxt);	 
int Languages (char* Arg[], int ArgCnt, char* MsgTxt);	
int OpSys (char* Arg[], int ArgCnt, char* MsgTxt);	
int FileCatalog (char* Arg[], int ArgCnt, char* MsgTxt);
int FileInfo (char* Arg[], int ArgCnt, char* MsgTxt); 
int Support_Undef (char* Arg[], int ArgCnt, char* MsgTxt);
int Support_Valid1stArg(int cmd, char * arg1, char* msg);

#define  ERROR_UNDEF_CMD "Undefined Command Type in Request\n"
#define  ERROR_BAD_ARG1 "Invalid Argument 1 (region) for Command Type\n"
#define  ERROR_BAD_ARGS "Invalid Arguments for Command Type\n"
#define  ERROR_BAD_ARGS2 "Invalid Arguments2 for Command Type\n"
#define  ERROR_CMD_FAILED "Command Failed\n"

#define DOSUPPORT "DoSupportCommand() Called\n"

  int (*support_cmdFp[16])() = 
	{ Support_Reset, CategoryListing, ProductListing, SearchProducts, 
	  DownloadCategories, Languages, OpSys, FileCatalog, 
	  FileInfo, Support_Undef, Support_Undef, Support_Undef, 
          Support_Undef, Support_Undef, Support_Undef, Support_Undef };

  int support_cmdArgCnt[16] = 
	{ 2,	 0,	1,	1,
	  1,	 0,	1,	4,
	  1, 	 0,	0,	0,
	  0,  	 0,	0,	0 };

#define VALID(v,n,x)  (((v>=n)&&(v<=x))?(1):(0))
			 

/*typedef struct _support_globals_t {
  int lastResetTime;
  int gflag;
  int Time;
  int Min_region;
  int Max_region;
  int Load;
  int Scaled_Load;
  int Num_Products;
  int Num_ProductLines;
  int Num_ModelLines;
  int Num_SearchResults;
  int region_range;
  char ResetDate[16];
} support_globals_t;*/

workload_globals_t Constants;
workload_globals_t *gSConstants = NULL;

char *LanguageList[30] =
{ "Arabic",     "Bulgarian", "Chinese-S",  "Chinese-T", "Czech",
  "Danish",     "Dutch",     "English",    "Estonian",  "Finnish",
  "French",     "German",    "Greek",      "Hebrew",    "Hungarian",
  "Indonesian", "Italian",   "Japanese",   "Korean",    "Norwegian",
  "Pan-Euro",   "Polish",    "Portuguese", "Russian",   "Slovak",
  "Slovenian",  "Spanish",   "Swedish",    "Thai",      "Turkish" };


char *OSList[10] =
{ "RNZX", "RN3K", "RN2045", "Enterprise Xilin V3.01", "Desktop Xilin V3.0",
  "OS 743 for Architecture N7", "NP-OS V13.41", "CafeOS 2.3.1 for HA82",
  "CafeOS 2.3.1 for NA90", "FreeBinOS 7.5" };

char *DnldCatList[20] =
{ "All", "Application", "Audio Drivers", "BIOS", "Chipset",
  "Communication Drivers", "Diagnostics", "IDE/SCSI", "Input Drivers", 
  "Keyboard  Drivers", 
  "Monitors", "Network Drivers", "Patches", "Removable Media Drivers", 
  "Security Patches",
  "Software Dev. Tools", "System Utilities", "System Management", 
  "Video Drivers", "Virus Protection" };

#ifndef BESIM_SUPPORT_GLOBALS_PATH
#ifndef WIN32
#define BESIM_SUPPORT_GLOBALS_PATH "/tmp/besim_support.globals"
#else
#define BESIM_SUPPORT_GLOBALS_PATH "c:\\besim_support.globals"
#endif
#endif


#endif /*BESIM_ECOMMERCE_H_INCLUDED*/
