#!/bin/bash
#
# Invocation: test_besim_ecom.sh <besim_script_request>
# Prerequisites: cURL (http://curl.haxx.se/) must be in the path.
#
# Example: test_besim_ecom.sh http://bsm614:81/fast-cgi/besim_fcgi.fcgi
# Example: test_besim_ecom.sh http://bsm614:81/isapi-bin/besim_zisapi.api
#

if [ ! -n "$1" ]
then
  echo "Usage:   `basename $0` http://<BeSim host>:<port>/<BeSim API>"
  echo "Example: `basename $0` http://bsm614:81/fcgi/besim_fcgi.fcgi"
  exit 1
fi

sample_queries=(
"?2&0&1079975569&1&80&100"
"?2&1&20&Seg1&Aone+2be+3c"

"?2&2&20&Home"

"?2&3&20&Home&PDA"

"?2&4&20&Home&FastOne"

"?2&5&20&Home&FastOne-23&2"

"?2&5&01&smb&LCD-TVs01&1"

"?2&6&20&FastOne-23&Option1=ABC-22&Option2=DEF-23"

"?2&7&20&M.Y.Self@home.net"

"?2&8&20&Mike&Self&&M.Y.Self@home.net&M.Y.Self@home.net"

"?2&9&20&M.Y.Self@home.net&2&FastOne-23&1&2&Option1=ABC-22&Option2=DEF-23&OtherOne-11&5&1&Option1=QAS-11"

"?2&9&20&M.Y.Self@home.net&4&FastOne-23&1&2&Option1=ABC-22&Option2=DEF-23&OtherOne-11&5&1&Option1=QAS-11&ThirdItem&9&0&FouthItem&4&4&Option1=123&Option2=asd&Option3=443539475&Option4=fred"

"?2&10&20&M.Y.Self@home.net&Mike&Self&123%20Elm%20St&Townville&VA&12345&Mike&Self&123%20Elm%20St&Townville&VA&12345&123-345-4567&GoodCred&6011234900001234&07&2005&3&FastOne-23&1&2&Option1=ABC-22&Option2=DEF-23&OtherOne-11&5&1&Option1=QAS-11&FastTwo-23&1&2&Option1=ABC-22&Option2=DEF-23b"

"?2&0&1079975569&1&80&100"

"?2&10&01%31%30%36%32%40%61%62%63%2e%63%6f%6d&%4a%6f%68%6e&%44%6f%65&%4d%61%67%6e%6f%6c%69%61%20%41%76%65&%4c%69%74%74%6c%65%74%6f%6e&%53%43&%30%33%38%32%31&%37%37%37%2d%36%36%37%2d%31%32%35%36&%4a%6f%68%6e&%44%6f%65&%43%6c%6f%75%64%54%72%65%65%20%41%76%65&%53%75%64%62%75%72%79&%4e%4a&%31%33%38%32%31&%33%33%33%2d%34%34%35%2d%35%36%37%38&%4d%61%73%74%65%72%63%61%72%64&%37%33%31%37%34%31%31%37%37%39%37%31%31%30%32%36&%34&%32%30%30%33&1&Projectors19&1&3&Very-Large-Computers-Model-0&Accessories-Model-1300-for-very-large-computers&Laptops-Model-1200-for-very-large-computers"

"?2&10&01%31%30%36%31%40%61%62%63%2e%63%6f%6d&%4a%6f%68%6e&%44%6f%65&%4d%61%67%6e%6f%6c%69%61%20%41%76%65&%4c%69%74%74%6c%65%74%6f%6e&%53%43&%30%33%38%32%31&%37%37%37%2d%36%36%37%2d%31%32%35%36&%4a%6f%68%6e&%44%6f%65&%43%6c%6f%75%64%54%72%65%65%20%41%76%65&%53%75%64%62%75%72%79&%4e%4a&%31%33%38%32%31&%33%33%33%2d%34%34%35%2d%35%36337%38&%4d%61%73%74%65%72%63%61%72%64&537%33%31%37%34%31%31%37%37%39%37%31%31%30%32%36&%34&%32%30%30%33&1&Projectors19&1&3&Computers-Model-0&Accessories-Model-1300&Laptops-Model-1222"
"?2&11"
)

echo -e "\nTesting BESIM Requests for Ecommerce Workload\n";

for i in "${sample_queries[@]}"
do
  echo -e "\n\n\n"
  echo -e "-----------------------------------------\n"
  echo -e "$i\n\n"
  curl -v "$1$i"
done
