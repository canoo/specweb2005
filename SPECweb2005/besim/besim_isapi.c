/*

    SPECweb2005 Backend Simulator (BESIM) ISAPI in C

    Calling Sequence:

    GET /isapi-bin/specweb2005_besim.api?<Workload_Type>&<Command_Type>&\
	<param=value>[&<param=value>]


    Returns:

	HTTP 200 OK
        Cache-control: no-store
	Content-type: text/html
	Content-Length: [length of all return text - excluding headers]

	<html>
	<head><title>SPECweb2005 BESIM</title></head>
	<body>
	<p>SERVER_SOFTWARE = [ServerSoftware]
	<p>REMOTE_ADDR = [RemoteAddr]
	<p>SCRIPT_NAME = [ScriptName]
	<p>QUERY_STRING = [QueryString]
	<pre>
	[BeSIM MessageText]
	</pre>
	</body></html>

*/

/*#include "fcgi_stdio.h"*/
#include "besim_common.h"

#include <httpext.h>
#ifndef WIN32
#include <wintypes.h>
#endif

#include "besim_isapi.h"

void DoOutput(char *Buf, int Len, EXTENSION_CONTROL_BLOCK *pEcb);
void ProcessQuery (char *Buf, char *Qstring);
int Besim_ReadFile(char *FileName, char *Buf, int BufSize);
void DoBankingCommand(char *Arg[],int ArgCnt, char* MsgText);
void DoEcommerceCommand(char *Arg[],int ArgCnt, char* MsgText);
void DoSupportCommand(char *Arg[],int ArgCnt, char* MsgText);

static int Inited = FALSE;
static spec_data_t Context, *pContext;
global_spec_data_t gConstants;



int Besim_main(WebInputs_t *WebInputs) {
  int count = 0;
  int len, BufLen;

  char ServerSoftware[128];
  char RemoteAddr[128];
  char ScriptName[256];
  char QueryString[1024];
  char TopDir[128];
  char RequestMethod[128];

/*  char FileName[PATH_MAX];*/
  char Buffer[BUFLEN];
  char MsgText[BUFLEN];
  EXTENSION_CONTROL_BLOCK *pEcb;

  pEcb = (EXTENSION_CONTROL_BLOCK *) WebInputs;

  /* Process Requests */


  /* Assign some handy pointers*/
  pContext = &Context;

  pContext->BufCurLen = 0;


  /* Get Environment Variables */
  BufLen = sizeof(ServerSoftware);
  pEcb->GetServerVariable( pEcb->ConnID, "SERVER_SOFTWARE",
			   ServerSoftware, &BufLen);

  BufLen = sizeof(RemoteAddr);
  pEcb->GetServerVariable( pEcb->ConnID, "REMOTE_ADDR",
			   RemoteAddr, &BufLen);

  BufLen = sizeof(ScriptName);
  pEcb->GetServerVariable( pEcb->ConnID, "SCRIPT_NAME",
                           ScriptName, &BufLen);

  BufLen = sizeof(QueryString);
  pEcb->GetServerVariable( pEcb->ConnID, "QUERY_STRING",
                           QueryString, &BufLen);

  BufLen = sizeof(RequestMethod);
  pEcb->GetServerVariable( pEcb->ConnID, "REQUEST_METHOD",
                           RequestMethod, &BufLen);

  /* Build Page Template from bolierplate and environment variables */

  BufLen = sizeof(TopDir);
  pEcb->GetServerVariable( pEcb->ConnID, "DOCUMENT_ROOT",
                           TopDir, &BufLen);

    if (TopDir == NULL) {
      len = sizeof(DEFAULT_TOP_DIR);
      strncpy(TopDir, DEFAULT_TOP_DIR, len);
      TopDir[len] = '\0';
    }


    len = sprintf(Buffer, BOILERPLATE_START, ServerSoftware, RemoteAddr,
		  ScriptName,QueryString);

    /* Process the query to obtain message text to complete the page */

    if (QueryString != NULL && strlen(QueryString) > 0) {
      ProcessQuery(MsgText, QueryString);
      strncpy(&Buffer[len], MsgText, strlen(MsgText));
      len += strlen(MsgText)-1;
    } else { /* no query string */
          sprintf(&Buffer[len], "%s%s", ERR1_STATUS, ERROR_NO_QUERY_STRING);
          len += sizeof(ERR1_STATUS)-1 ;
          len += sizeof(ERROR_NO_QUERY_STRING)-2 ;
    }

    DoOutput(Buffer, len, pEcb);
    return 0;
}

void ProcessQuery (char *Buf, char *Qstring) {
  int WorkloadType;
  int CommandType;
  char *pStr;
  char *Arg[100];
  int ArgCnt;

  /* Place each ampersand separated argument into the Arg list */

  Arg[0] = Qstring;
  for (ArgCnt = 0, pStr = Qstring; *pStr != '\0'; pStr++) {
    if ('&' == *pStr) {
      *pStr = '\0';
      ArgCnt++;
      Arg[ArgCnt] = pStr + 1;
    }
    else if ( ',' == *pStr) {
      *pStr = '\0';
      ArgCnt++;
      Arg[ArgCnt] = pStr + 1;
    }
  }

  WorkloadType = atoi(Arg[0]);
  CommandType = atoi(Arg[1]);

  /* Select and process workload command */

  switch (WorkloadType) {
case BANKING:
    DoBankingCommand(Arg,ArgCnt,Buf);
    break;
case ECOMMERCE:
    DoEcommerceCommand(Arg,ArgCnt,Buf);
    break;
case SUPPORT:
    DoSupportCommand(Arg,ArgCnt,Buf);
    break;
default:
    sprintf(Buf, "%s%s", ERR1_STATUS, ERROR_UNDEF_WORKLOAD);
    break;
  }

}

void DoOutput(char *Buf, int Len, EXTENSION_CONTROL_BLOCK *pEcb) {

  char Header[128];
  int HeaderLen;
  int TempLen;



  /* Append last piece of HTML boilerplate to page */

  TempLen = strlen(BOILERPLATE_END) + 1;
  memcpy(&Buf[Len], BOILERPLATE_END, TempLen);
  Len += TempLen - 1;

  /* Add HTTP headers to output */

  sprintf(Header,
   "Cache-control: no-store\r\nContent-type: text/html\r\nContent-Length: %d\r\n\r\n",
   Len);

  HeaderLen = strlen(Header);

  /* Send completed response back through ISAPI interface */

  /*printf ("%s%s",Intro, Buf);*/

  pEcb->ServerSupportFunction( pEcb->ConnID,
				       HSE_REQ_SEND_RESPONSE_HEADER, 0,
				       &HeaderLen, (DWORD *) Header);

  TempLen = strlen(Buf);
  pEcb->WriteClient( pEcb->ConnID, Buf, &TempLen, 0);


}

int Besim_ReadFile(char *FileName, char *Buf, int BufSize) {

  int Desc;
  int Len;

  if ( (Desc = open(FileName, O_RDONLY, NULL)) == -1)
    return sprintf(Buf, "Error opening file '%s': %s", FileName,
		   strerror(errno));
  else {
    Len = read (Desc, (void *) Buf, BufSize);
    close(Desc);
    return Len;
  }
}

/****************ISAPI*****************/

/*
 * FUNCTION:	BOOL WINAPI GetExtensionVersion(HSE_VERSION_INFO *pVer)
 *
 * PURPOSE:	ISAPI call that gets executed once when the calling
 *		process loads this library
 *
 * ARGUMENTS:	HSE_VERSION_INFO  *pVer  passed in structure for setting
 *					 version and description
 *
 * RETURNS:	TRUE
 *
 * COMMENTS:	For more info on ISAPI coding refer to
 *		http://www.microsoft.com/WIN32DEV/APIEXT/ISAPIMRG.HTM
 *
 */
BOOL WINAPI
GetExtensionVersion (HSE_VERSION_INFO *pVer) {
  pVer->dwExtensionVersion = HSE_VERSION_MAJOR;
  strcpy (pVer->lpszExtensionDesc, "SPECweb2005 BESIM");

  return TRUE;
}


/*
 * FUNCTION:	DWORD WINAPI HttpExtensionProc(EXTENSION_CONTROL_BLOCK *pEcb)
 *
 * PURPOSE:	ISAPI call that gets executed for every client request to
 *		this library.  This is ISAPI's equivalent to main().
 *
 * ARGUMENTS:	EXTENSION_CONTROL_BLOCK  *pEcb
 *					passed in structure with variables
 *					and function pointers
 *
 * RETURNS:	HSE_STATUS_SUCCESS, HSE_STATUS_ERROR
 *
 * COMMENTS:	For more info on ISAPI coding refer to
 *		http://www.microsoft.com/WIN32DEV/APIEXT/ISAPIMRG.HTM
 *
 */

DWORD WINAPI
HttpExtensionProc (EXTENSION_CONTROL_BLOCK *pEcb) {


  return Besim_main((WebInputs_t *)pEcb);
}

