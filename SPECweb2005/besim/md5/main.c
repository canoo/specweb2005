/**
 * Code to calculate MD5 checksums for a set of files. This code is based on the
 * code provided in appendix A of RFC 1321.
 */

#include <stdio.h>
#include "md5.h"

/**
 * Calculate MD5 digest for a file with the given name.
 */
void MD5(unsigned char *passwd)
{
	// Variables
	MD5_CTX context;
	int i;
	unsigned char digest[16];

		MD5Init(&context);
		MD5Update(&context, passwd, strlen(passwd));
		MD5Final(digest, &context);
		printf("MD5 (%s) = ", passwd);
		for(i=0; i<16; i++)
			printf("%02x", digest[i]);
		printf("\n");
}

/**
 * Calculates MD5 digest for a set of files.
 */
int main(int argc, char *argv[])
{	
	// Variables
	int i;
	
	// Check parameters
	if(argc!=2)
	{
		printf("Usage: %s <string>\n", argv[0]);
		return 0;
	}
	
	// Calculate digest
	for(i=1; i<argc; i++)
		MD5((unsigned char *)argv[i]);
	return 0;
}

