#!/bin/bash
#
# Invocation: test_besim_bank.sh <besim_script_request>
# Prerequisites: cURL (http://curl.haxx.se/) must be in the path.
#
# Example: test_besim_bank.sh http://bsm614:81/fast-cgi/besim_fcgi.fcgi
# Example: test_besim_bank.sh http://bsm614:81/isapi-bin/besim_zisapi.api
#

if [ ! -n "$1" ]
then
  echo "Usage:   `basename $0` http://<BeSim host>:<port>/<BeSim API>"
  echo "Example: `basename $0` http://bsm614:81/fcgi/besim_fcgi.fcgi"
  exit 1
fi  

sample_queries=(
"?1&0&1097157010&1&2000&200&/www/bank/images&0"
"?1&1&88"
"?1&2&18"
"?1&3&157"
"?1&4&58&3"
"?1&5&38"
"?1&6&78&1&Insurance%20Inc.&8012%20Birch%20Ln&Middletown&NC&55566&123-555-1212"
"?1&7&149&1&2004-10-09&100.00"
"?1&8&38&2004057&20041007"
"?1&9&157"
"?1&10&81&2347%20Birch%20Ln&Littleton&PA&13919&customer770%40isp.net&phone%3D123-555-1212"
"?1&11&327&0866436362&20041007&600"
"?1&12&177&0807761921&50&0832942271&20041007"
)

echo -e "\nTesting BESIM Requests for Banking Workload\n";

for i in "${sample_queries[@]}"
do
  echo -e "\n\n\n"
  echo -e "-----------------------------------------\n"
  echo -e "$i\n\n"
  curl -v "$1$i"
done
