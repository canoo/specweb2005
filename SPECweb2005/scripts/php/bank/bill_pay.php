<?php
  require('inc.common.php');
  
  
  function backend_get_bill_pay($userid){
    $request=BACKEND_CMD_BILL_PAY.'&'.$userid;
    list($r, $errno)=backend_get_array($request);
    for($i=0; $i<count($r); $i++){
      $r[$i][2]=date_string($r[$i][2]);
    }
    return $r;
  }
  
  check_login();
  $userid=$_SESSION['userid'];
  $result=backend_get_bill_pay($userid);
  $smarty=new SmartyBank;
  $smarty->assign('userid', $userid);
  $smarty->assign('bill_pay', $result);
  $smarty->display('bill_pay.tpl');
?>

  
