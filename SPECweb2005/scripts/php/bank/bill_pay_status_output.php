<?php
  require('inc.common.php');
  
  function backend_bill_pay_status($userid, $start, $end){
    $request=BACKEND_CMD_REVIEW_BILL_PAY_STATUS.'&'.$userid.'&'.
             $start.'&'.$end;
    $r=backend_get_array($request);
    return $r;
  }
  
  check_login();
  $userid=$_SESSION['userid'];
  if(empty($_GET['start']) || !($start=date_to_int_string(trim($_GET['start'])))){
    show_msg('illegal start date');
    exit();
  }
  if(empty($_GET['end']) || !($end=date_to_int_string(trim($_GET['end'])))){
    show_msg('illegal end date');
    exit();
  }
  list($status, $errno)=backend_bill_pay_status($userid, $start, $end);
  if($errno==0){
    $smarty=new SmartyBank;
    $smarty->assign('userid', $userid);
    $smarty->assign('status', $status);
    $smarty->display('bill_pay_status_output.tpl');
  }else{
    show_msg("Error Code: $errno");
  }
?>
