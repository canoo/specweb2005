<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" 
"http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <title>SPECweb2005: Bill Pay</title>
  </head>
  <body bgcolor="white">
  <!-- SPECweb2005 Dynamic Data Area -->
  <table  summary="SPECweb2005_User_Id">
    <tr><th>User ID</th></tr>
    <tr><td>{$userid}</td></tr> 
  </table>
    <table summary="SPECweb2005_Last_Payment" cellpadding=3 border=1>
      <tr>
        <th>Payee</th>
        <th>Date</th>
        <th>Amount</th>
	</tr>
      {foreach key=index item=payee from=$bill_pay}
        <tr>
        <td>{$payee[0]}</td>
        <td>{$payee[2]}</td>
        <td>{$payee[1]}</td>
        </tr>
      {/foreach}
    </table>
  <!-- SPECweb2005 Displayable Page Title -->
  <table>
    <tr>
      <td><b><font color="#9400d3">SPECweb2005: Bill Pay</font></b></td>
    </tr>
  </table>
  <!-- SPECweb2005 User Action Area -->
  {include file="menu.tpl"}
    <form method="post" action="quick_pay.php">
    <b>Schedule for next payment</b>
    <table cellpadding=3 border=1>
      <tr>
        <th>Payee</th>
        <th>Date</th>
        <th>Amount</th>
	</tr>
      <tr>
        <td><input type="text" name="payee[0]"></td>
        <td><input type="text" name="date[0]"></td>
        <td><input type="text" name="amount[0]"></td>
	</tr>
      <tr>
        <td><input type="text" name="payee[1]"></td>
        <td><input type="text" name="date[1]"></td>
        <td><input type="text" name="amount[1]"></td>
	</tr>
      <tr>
        <td><input type="text" name="payee[2]"></td>
        <td><input type="text" name="date[2]"></td>
        <td><input type="text" name="amount[2]"></td>
	</tr>
      <tr>
        <td><input type="text" name="payee[3]"></td>
        <td><input type="text" name="date[3]"></td>
        <td><input type="text" name="amount[3]"></td>
 	</tr>
	<tr>
        <td><input type="text" name="payee[4]"></td>
        <td><input type="text" name="date[4]"></td>
        <td><input type="text" name="amount[4]"></td>
	</tr>
    </table>
      <b>Note:</b> Date in yyyy-mm-dd format (ISO-8601).<br>
    <input type="submit" value="Submit">
    </form>
  
  <!-- SPECweb2005 Image References -->
  <!-- SPECweb2005 Embedded Text -->
<pre>   
{include file=$PADDING_DIR|cat:"bill_pay"}
</pre>
  </body>
</html>
    
      
