<?php

/* Copyright (c) 2004-2006 Standard Performance Evaluation Corporation (SPEC)
 *               All rights reserved.
 *
 * This source code is provided as is, without any express or implied warranty.
 */

  require('inc.common.php');


  check_login();
  $userid=$_SESSION['userid'];
  $check_no=$_GET['check_no'];
  if(empty($check_no)){
    show_msg('Empty check number');
    exit();
  }
  list($check_image, $errno)=backend_query_check($userid, $check_no);
  $smarty=new SmartyBank;
  if($errno==0){
    $smarty->assign('userid', $userid);
    $smarty->assign('account_no', $check_image[0][0]);
    $smarty->assign('check_no', $check_no);
    $smarty->display('check_detail_html.tpl');
  }else{
    show_msg("Error Code: $errno");
  }
?>


