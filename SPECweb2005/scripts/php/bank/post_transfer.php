<?php

/* Copyright (c) 2004-2006 Standard Performance Evaluation Corporation (SPEC)
 *               All rights reserved.
 *
 * This source code is provided as is, without any express or implied warranty.
 */

  require('inc.common.php');

  function backend_post_transfer($userid, $from, $to, $amount){
    $request=BACKEND_CMD_POST_TRANSFER.'&'.$userid.'&'.
             rawurlencode($from).'&'.$amount.'&'.
             rawurlencode($to).'&'.strftime('%Y%m%d');
    $r=backend_get_array($request);
    return $r;
  }

  check_login();
  $userid=$_SESSION['userid'];
  $smarty=new SmartyBank;
  $from=$_POST['from'];
  $to=$_POST['to'];
  if(empty($_POST['amount']) || !is_numeric($_POST['amount'])){
    show_msg("Invalid Amount");
    exit();
  }
  $amount=(float)$_POST['amount'];
  if($amount<=0.0){
    show_msg("Amount must be greater than 0");
    exit();
  }
  if($to==$from){
    show_msg("Transfer from and to the same account not allowed");
    exit();
  }
  list($acct_balance, $errno)=backend_post_transfer($userid, $from, $to, $amount);
  if($errno==0){
    $smarty->assign('userid', $userid);
    $smarty->assign('acct_balance', $acct_balance);
    $smarty->assign('to', $to);
    $smarty->assign('from', $from);
    $smarty->assign('amount', $amount);
    $smarty->display('post_transfer.tpl');
  }else{
    show_msg("Error Code: $errno");
  }
?>

