<?php
  require('inc.common.php');
  
  function backend_change_profile($userid, $address, $city, $state, $zip, $phone, $email){
    $address=rawurlencode($address);
    $city=rawurlencode($city);
    $state=rawurlencode($state);
    $zip=rawurlencode($zip);
    $phone=rawurlencode($phone);
    $email=rawurlencode($email);
	$request=BACKEND_CMD_CHANGE_PROFILE.'&'.($userid).'&'.($address).'&'.($city).'&'.($state).'&'.($zip).'&'.($email).'&'.($phone);
	$r=backend_get_array($request);
    return $r;
  }
  
  check_login();
  $smarty=new SmartyBank;
  if(empty($_POST['address']) || 
     empty($_POST['city']) ||
     empty($_POST['state']) ||
     empty($_POST['zip']) ||
     empty($_POST['phone']) ||
     empty($_POST['email'])){
     $msg="Profile update failed: you submitted empty fields";
  }else{
     list($status, $errno)=backend_change_profile($_SESSION['userid'],
                            $_POST['address'],
                            $_POST['city'],
                            $_POST['state'],
                            $_POST['zip'],
                            $_POST['phone'],
                            $_POST['email']);
  }
  $smarty->assign('userid', $_SESSION['userid']);
  if($errno) {
    $smarty->assign('msg', "Update profile failed");
  } else {
    $smarty->assign('msg', 'Profile Changed.');
  }
  $smarty->assign('conf', $status);
  $smarty->display('change_profile.tpl');
?>
