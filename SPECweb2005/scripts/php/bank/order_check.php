<?php

/* Copyright (c) 2004-2006 Standard Performance Evaluation Corporation (SPEC)
 *               All rights reserved.
 *
 * This source code is provided as is, without any express or implied warranty.
 */

  require('inc.common.php');

  check_login();
  $userid=$_SESSION['userid'];
  $smarty=new SmartyBank;
  $smarty->assign('userid', $userid);
  $smarty->assign('acct_balance', backend_get_acct_balance($userid));
  $smarty->display('order_check.tpl');
?>
