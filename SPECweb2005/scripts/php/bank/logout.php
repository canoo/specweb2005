<?php

/* Copyright (c) 2004-2006 Standard Performance Evaluation Corporation (SPEC)
 *               All rights reserved.
 *
 * This source code is provided as is, without any express or implied warranty.
 */

  require('inc.common.php');
  $userid='';
  if(isset($_SESSION['userid'])){
    $userid=$_SESSION['userid'];
  }
  $_SESSION=array();
  session_destroy();
  $smarty=new SmartyBank;
  $smarty->assign('userid', $userid);
  $smarty->display('logout.tpl');
?>
