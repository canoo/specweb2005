<?php

/* Copyright (c) 2004-2006 Standard Performance Evaluation Corporation (SPEC)
 *               All rights reserved.
 *
 * This source code is provided as is, without any express or implied warranty.
 */

  require('inc.common.php');

  function backend_get_profile($userid){
	$request=BACKEND_CMD_PROFILE.'&'.($userid);
	list($a, $errno)=backend_get_array($request);
        if($errno==0)
          $profile=array('address'=>$a[0][0],
		   'city'=>$a[0][1],
		   'state'=>$a[0][2],
		   'zip'=>$a[0][3],
                   'phone'=>$a[0][5],
                   'email'=>$a[0][4]);
    return array($profile, $errno);
  }
  check_login();
  $userid=$_SESSION['userid'];
  $smarty=new SmartyBank;
  list($profile, $errno)=backend_get_profile($userid);
  if($errno==0){
    $smarty->assign('userid', $userid);
    $smarty->assign('profile', $profile);
    $smarty->display('profile.tpl');
  }else{
    show_msg("Error Code: $errno");
  }
?>
