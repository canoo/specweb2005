<?PHP

/* Copyright (c) 2004-2006 Standard Performance Evaluation Corporation (SPEC)
 *               All rights reserved.
 *
 * This source code is provided as is, without any express or implied warranty.
 */

	// Include required objects and functions
	require_once('inc.common.php');

	// Redirect to home page if required information is not present
    checkGET('id');

	// Redirect to product page if required information is not present
	if( !isset($_GET['category']) || !isset($_GET['os']) || !isset($_GET['lang']) ){
		header('Location: product.php?id=' . $_GET['id']);
		exit();
	}

	// Include required objects and functions
	require_once('inc.common.php');

	// Initalize template engines
	$frameTemplate = new SmartySupport;
	$bodyTemplate  = new SmartySupport;

	// Connect to the datasource
    $file_listing = backend_query(BACKEND_CMD_FILE_LISTING .'&'. $_GET['id'] .'&'. $_GET['category'] .'&'. $_GET['os'] .'&'. $_GET['lang']);

	// Populate Templates
	$bodyTemplate->assign('files', $file_listing);

	$frameTemplate->assign('title', "Product $_GET[id] - Files");
	$frameTemplate->assign('body', $bodyTemplate->fetch('page.fileCatalog.htm'));

    if(is_file(PADDING_DIR . 'fileCatalog'))
        $frameTemplate->assign('padding',  file_get_contents(PADDING_DIR . 'fileCatalog'));
    else
        echo('Unable to locate padding file.');

	// Render the page to the browser
	$frameTemplate->display('tpl.main.htm');
?>
