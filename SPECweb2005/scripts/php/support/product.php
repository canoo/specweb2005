<?PHP

/* Copyright (c) 2004-2006 Standard Performance Evaluation Corporation (SPEC)
 *               All rights reserved.
 *
 * This source code is provided as is, without any express or implied warranty.
 */

	// Include required objects and functions
	require_once('inc.common.php');

	// Redirect to home page if required information is not present
    checkGET('id');

	// Initalize template engines
	$frameTemplate = new SmartySupport;
	$bodyTemplate  = new SmartySupport;

	// Connect to the datasource
    $download_categories = backend_query(BACKEND_CMD_DOWNLOAD_CATEGORIES . '&' . $_GET['id']);
    $languages = backend_query(BACKEND_CMD_LANGUAGES);
    $operating_systems = backend_query(BACKEND_CMD_OPERATING_SYSTEMS . '&' . $_GET['id']);

	// Populate Templates
	$bodyTemplate->assign('categories', $download_categories);
	$bodyTemplate->assign('langList',   $languages);
	$bodyTemplate->assign('osList',     $operating_systems);

	$frameTemplate->assign('title', "Product $_GET[id]");
	$frameTemplate->assign('body', $bodyTemplate->fetch('page.product.htm'));

    if(is_file(PADDING_DIR . 'product'))
        $frameTemplate->assign('padding',  file_get_contents(PADDING_DIR . 'product'));
    else
        echo('Unable to locate padding file.');

	// Render the page to the browser
	$frameTemplate->display('tpl.main.htm');
?>
