<?PHP

/* Copyright (c) 2004-2006 Standard Performance Evaluation Corporation (SPEC)
 *               All rights reserved.
 *
 * This source code is provided as is, without any express or implied warranty.
 */

	require_once('inc.common.php');

	// Initalize template engines
	$frameTemplate = new SmartySupport;
	$bodyTemplate  = new SmartySupport;

	// Connect to the datasource
    $categories = backend_query(BACKEND_CMD_CATEGORIES);

	// Populate Templates
	$bodyTemplate->assign('productCategories', $categories);

	$frameTemplate->assign('title', 'Home');
	$frameTemplate->assign('body', $bodyTemplate->fetch('page.index.htm'));

    if(is_file(PADDING_DIR . 'index'))
        $frameTemplate->assign('padding',  file_get_contents(PADDING_DIR . 'index'));
    else
        echo('Unable to locate padding file.');

	// Render the page to the browser
	$frameTemplate->display('tpl.main.htm');

?>
